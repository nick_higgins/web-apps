GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO tomcat;

DROP TABLE wishlist;
DROP TABLE messages;
DROP TABLE items;
DROP TABLE users;
DROP TABLE address;
DROP TYPE currency;

CREATE TYPE currency AS ENUM 
(
	'GBP', 
	'EUR', 
	'USD',
	'AUD'
);

CREATE TABLE address
(
	addrID				UUID PRIMARY KEY,
	addrStreet			VARCHAR(256),
	addrCity			VARCHAR(64),
	addrState			VARCHAR(8),
	addrCountry			VARCHAR(64),
	addrPostalCode		INT
);

CREATE TABLE users
(
	username			VARCHAR(128) PRIMARY KEY,
	password			VARCHAR(256) NOT NULL,
	salt				VARCHAR(256) NOT NULL,
	email				VARCHAR(128) NOT NULL,
	nickname			VARCHAR(64),
	firstname			VARCHAR(64),
	lastname			VARCHAR(64),
	yob					INT,
	confirm				BOOLEAN,
	banned				BOOLEAN,
	creditcard			VARCHAR(24),
	address				UUID REFERENCES address
);

CREATE TABLE items
(
	itemId				UUID PRIMARY KEY,
	owner				VARCHAR(128)  NOT NULL REFERENCES users,
	title				VARCHAR(128) NOT NULL,
	category			VARCHAR(64),
	picture				VARCHAR(256),
	description			VARCHAR(1024),
	address				UUID REFERENCES address,
	reservePrice		FLOAT,
	biddingStartPrice	FLOAT CHECK (biddingStartPrice >= 0),
	biddingIncrements	FLOAT CHECK (biddingIncrements >= 0),
	biddingCurrency		currency,
	endTime				TIMESTAMP,
	isInAuction			BOOLEAN,
	isSold				BOOLEAN,
	currentBiddingPrice	FLOAT,
	currentBidder		VARCHAR(128) REFERENCES users
);

CREATE TABLE wishlist
(
	username			VARCHAR(128) NOT NULL REFERENCES users,
	itemId				UUID NOT NULL REFERENCES items
);

CREATE TABLE messages
(
	msgId				UUID PRIMARY KEY,
	username			VARCHAR(128) NOT NULL REFERENCES users,
	generationTime		TIMESTAMP NOT NULL,
	title				TEXT NOT NULL,
	message				TEXT NOT NULL,
	unread				BOOLEAN NOT NULL
);