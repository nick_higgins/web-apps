INSERT into address values (
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	'770 Eastern Parkway',
	'Sydney',
	'NSW',
	'Australia',
	'2000');
	
INSERT into users values (
	'admin',
	'1F9E11F46229033B4E832A209571E11374CF8409FF03EEAD691E33869F79B539',
	'48994C0B6F38DBA63E7A993320A83ECBED46C0F6B8C9F0168AE23E78CA696CD8757E4310399AA97F1180EE580E5151C05498D2343CA4C0EE28BB25C775195001',
	'nwhi705@cse.unsw.edu.au',
	'niggins',
	'Nicholas',
	'Higgins',
	'1992',
	'true',
	'false',
	'1234-5678-9012-3456',
	'bdc8ff20-c978-4289-abfa-24772f4fa731'
	);
	
INSERT into users values (
	'test',
	'09F027B838E9FD99125C2199BB8505710ED8D61C204BA6A8AE1ED7D044EAD704',
	'042FBF92629116DD14C04B4F10D411FEBD4D152D51C1F465CC30DF1DD0933AB38A1E486336C14A887602A53BA323991E931632AE5E6936E2BBA0ED660D51481C',
	'nwhi705@cse.unsw.edu.au',
	'niggins',
	'Nicholas',
	'Higgins',
	'1992',
	'true',
	'false',
	'1234-5678-9012-3456',
	'bdc8ff20-c978-4289-abfa-24772f4fa731'
	);
	
INSERT into users values (
	'test2',
	'09F027B838E9FD99125C2199BB8505710ED8D61C204BA6A8AE1ED7D044EAD704',
	'042FBF92629116DD14C04B4F10D411FEBD4D152D51C1F465CC30DF1DD0933AB38A1E486336C14A887602A53BA323991E931632AE5E6936E2BBA0ED660D51481C',
	'tim.j.masters@gmail.com',
	'tim',
	'Timothy',
	'Masters',
	'1987',
	'true',
	'false',
	'1234-5678-9012-3456',
	'bdc8ff20-c978-4289-abfa-24772f4fa731'
	);

INSERT INTO items values (
	'8b92982d-460c-435a-af58-b74c39236821',
	'test',
	'MacBook Pro 2015',
	'Computers',
	'http://static.trustedreviews.com/94/00002d1cb/93cd/MacBook-Air.jpg',
	'Cool and useless notebook',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	1026.00,
	100.00,
	10.00,
	'AUD',
	'2015-05-11 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO items values (
	'60e64d38-ab49-4974-8bb5-aa64a989dddd',
	'test2',
	'MacBook Pro 2008',
	'Computers',
	'https://d3nevzfk7ii3be.cloudfront.net/igi/5RIvmWVeAKJacKyM.larg',
	'Old notebook',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	613.00,
	100.00,
	10.00,
	'AUD',
	'2015-05-13 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO items values (
	'407f8813-412a-4f1e-b50b-190aa9f9bbf4',
	'test',
	'Nexus 6',
	'Phones',
	'http://www.androidpolice.com/wp-content/uploads/2014/09/nexus2cee_n6lf3.png',
	'Googles Big Phone',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	800.00,
	400.00,
	10.00,
	'AUD',
	'2015-05-16 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO items values (
	'40e8ddc6-c506-400b-af7a-4f7ec9ef3de0',
	'test2',
	'Nexus 5',
	'Phones',
	'http://cdn1.xda-developers.com/devdb/deviceForum/screenshots/2589/20131018T122945.png',
	'Googles Medium Phone',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	600.00,
	400.00,
	10.00,
	'AUD',
	'2015-05-13 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO items values (
	'a48a022b-7611-43a0-aa3d-68a05bf93c93',
	'test',
	'Nexus 4',
	'Phones',
	'http://stock-wallpapers.com/wp-content/uploads/2014/06/LG-Nexus-4.jpg',
	'Googles Small Phone',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	400.00,
	200.00,
	10.00,
	'AUD',
	'2015-05-11 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO items values (
	'bd6479cf-56dc-402e-84c8-838d389bfe53',
	'test2',
	'Surface Pro 3',
	'Tablets',
	'http://dri1.img.digitalrivercontent.net/Storefront/Company/msapac/images/English/en-APAC-Surface-64GB-i3-4YM-00007/en-APAC-L-Surface-64GB-i3-4YM-00007-mnco.jpg',
	'Microsofts Tablet Computer',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	1200.00,
	800.00,
	10.00,
	'AUD',
	'2015-05-11 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO items values (
	'cc01bc02-fcf4-4f06-a4bd-57678c8cd585',
	'test',
	'Pebble Watch',
	'Watches',
	'http://upload.wikimedia.org/wikipedia/commons/5/5e/Pebble_watch_trio_group_04.png',
	'Pebbles first smart watch',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	100.00,
	50.00,
	10.00,
	'AUD',
	'2015-05-11 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO items values (
	'7f091ffa-23b4-49f6-80bf-cb7071cbf87b',
	'test2',
	'Pebble Steel',
	'Watches',
	'https://blog.getpebble.com/wp-content/uploads/2014/01/140106C.Steel-Trio.png',
	'Pebbles updated smart watch',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	200.00,
	100.00,
	10.00,
	'AUD',
	'2015-05-11 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO items values (
	'a94c7962-7200-4dd1-871d-cec0a72fe0b0',
	'test',
	'Moto 360',
	'Watches',
	'http://phandroid.s3.amazonaws.com/wp-content/uploads/2014/03/Moto360__Metal_RGB.jpg',
	'Motorolas Android Wear Smart Watch',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	200.00,
	100.00,
	10.00,
	'AUD',
	'2015-05-11 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO items values (
	'e3870457-65fb-411d-b76c-4aa5c4cd0e92',
	'test',
	'Dell XPS 15',
	'Computers',
	'http://www.notebookcheck.net/uploads/tx_nbc2/xps15_gesamt4f_08_1.jpg',
	'Dells powerful laptop',
	'bdc8ff20-c978-4289-abfa-24772f4fa731',
	1000.00,
	500.00,
	25.00,
	'AUD',
	'2015-09-24 06:00:00',
	false,
	false,
	0.00,
	null	
);

INSERT INTO wishlist (username, itemId) values 
	('test', 'e3870457-65fb-411d-b76c-4aa5c4cd0e92'),
	('test', '7f091ffa-23b4-49f6-80bf-cb7071cbf87b'),
	('test', 'a94c7962-7200-4dd1-871d-cec0a72fe0b0');

INSERT INTO messages (msgId, username, generationTime, title, message, unread) values 
	('eaa4ef18-f3e3-11e4-b9b2-1697f925ec7b', 'test', '2015-05-06 21:00:00.00', 'Header', 'Hello World', 'false'), 
	('eaa4f30a-f3e3-11e4-b9b2-1697f925ec7b', 'test', '2015-05-06 21:01:00.00', 'Header', 'Test 1', 'true'),
	('eaa4f58a-f3e3-11e4-b9b2-1697f925ec7b', 'test', '2015-05-06 21:02:00.00', 'Header', 'Test 2', 'true'),
	('eaa4f7e2-f3e3-11e4-b9b2-1697f925ec7b', 'test', '2015-05-06 21:03:00.00', 'Header', 'Test 3', 'true'),
	('eaa4fe7c-f3e3-11e4-b9b2-1697f925ec7b', 'test', '2015-05-06 21:04:00.00', 'Header', 'Test 4', 'true')