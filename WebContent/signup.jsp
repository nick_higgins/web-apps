<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>        
<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="/css/global.css" />" rel="stylesheet">
<script src="<c:url value="/js/jquery-1.11.2.min.js" />"></script>
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<script src="<c:url value="/js/bootstrap.min.js" />"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sign Up</title>
<jsp:include page="Header.jsp"/>
</head>
<body>
	<div>
	  <form class="center" method="post" id="signUpForm" action="signup">
  		<input type="hidden" name="signup" />
	    <input style="width: 300px; margin: 10px auto;" class="form-control center" type='text' placeholder="Username" autofocus="autofocus" name="username"/>
	    <input style="width: 300px; margin: 10px auto;" class="form-control center" type='text' placeholder="Email" name="email"/>
		<input style="width: 300px; margin: 10px auto;" class="form-control center" type='password' placeholder="Password" name="password"/>
	    <input style="width: 300px; margin: 10px auto;" class="form-control center" type='password' placeholder="Password Check" name="passwordCheck"/>
	    <input style="width: 300px; margin: 10px auto; display: block" class="center btn btn-danger" type="submit" value="Sign Up"/>
	  </form>
	</div>
</body>
</html>