<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<link href="css/global.css" rel="stylesheet">
<script src="js/jquery-1.11.2.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="${pageContext.request.getContextPath()}/">Auction House</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <form class="navbar-form navbar-left" action="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search" name="search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
      	<c:if test="${empty sessionScope.username}">
            <li><a href="${pageContext.request.getContextPath()}/login">Log In</a></li>		
		</c:if>
		<c:if test="${not empty sessionScope.username}">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">${sessionScope.username} <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
	         <c:if test="${sessionScope.username ne 'admin'}">
	          	<li><a href="${pageContext.request.getContextPath()}/myhome">My Home</a></li>
	    	    <li><a href="${pageContext.request.getContextPath()}/wishlist">My Wishlist</a></li>
		        <li><a href="${pageContext.request.getContextPath()}/add">Add Item</a></li>
	            <li><a href="${pageContext.request.getContextPath()}/messages">Messages</a></li>            
	            <li class="divider"></li>            
	            <li><a href="${pageContext.request.getContextPath()}/profile">Update Profile</a></li>
	            <li class="divider"></li>            
	           </c:if>
            <li><a href="${pageContext.request.getContextPath()}/login?logout">Log Out</a></li>
          </ul>
        </li>
		</c:if>
      </ul>
         <c:if test="${sessionScope.username eq 'admin'}">
		      <form class="navbar-form navbar-right" action="admin">
				<input type='hidden' name='ban' />
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="User" name="username">
		        </div>
		        <button type="submit" class="btn btn-default">Ban</button>
		      </form>
		</c:if>		      
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!-- Notification Stuff -->
<script>
function poll(){
   setTimeout(function(){
	   if ($("#notify").is(":visible") || "${sessionScope.username}" == ""){
		   poll();
	   } else {
	      $.ajax({ url: "messages?unread&username=${sessionScope.username}", success: function(data){
	  		if (data != undefined){
		  		console.log("Data: " + data);
		      	$("#notify").html("<a href='#' class='close' onclick='closeAlert()'>&times;</a>" + data);
		  		$("#notify").show();
		  	}
          }});
	      poll();
	   }
  }, 2000);
};
function closeAlert(){
	console.log("Close notify");
    $("#notify").hide();	
}
poll();
</script>
<div id="errors" style="margin:10px 10px;">
	<c:if test='${not empty error}'>
		<div class="alert alert-danger">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
		    ${error}
		</div>
	</c:if>
	<c:if test='${not empty success}'>
		<div class="alert alert-success">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
			${success}
		</div>
	</c:if>
	<c:if test='${not empty info}'>
		<div class="alert alert-info">
		    <a href="#" class="close" data-dismiss="alert">&times;</a>
			${info}
		</div>
	</c:if>
	<div class="alert alert-info" style="display:none;" id="notify">
	    <a href="#" class="close" onclick="closeAlert()">&times;</a>
	</div>
</div>