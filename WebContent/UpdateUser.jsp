<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update User</title>
<jsp:include page="Header.jsp"/>
</head>
<body>
<div class="container">
	<form action='profile' method="post">
   	  	<input type="hidden" name="update" value="update"/>
		<c:if test="${not empty addr}">
			<input type="hidden" name="addrId" value="${addr.uuid}"/>
		</c:if>
		<div class="row">
			<div class="col-md-4">
				<label for="email">Email</label>
				<input type="text" class="form-control" name="email" value="${user.email}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="password">Password</label>
				<input type="password" class="form-control" name="password" value=""/>
			</div>
			<div class="col-md-4">
				<label for="passwordCheck">Password Check</label>
				<input type="password" class="form-control" name="passwordCheck" value=""/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="firstname">First Name</label>
				<input type="text" class="form-control" name="firstname" value="${user.firstname}"/>
			</div>
			<div class="col-md-4">
				<label for="lastname">Last Name</label>
				<input type="text" class="form-control" name="lastname" value="${user.lastname}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="nickname">Nickname</label>
				<input type="text" class="form-control" name="nickname" value="${user.nickname}"/>
			</div>
			<div class="col-md-4">
				<label for="yob">Year of Birth</label>
				<input type="number" class="form-control" name="yob" value="${user.yob}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="creditcard">Credit Card</label>
				<input type="text" class="form-control" name="creditcard" value="${user.creditcard}"/>
			</div>	
		</div>
		<div class="row"></div>
		<div class="row">
			<div class="col-md-4">
				<label for="street">Street Address</label>
				<input type="text" class="form-control" name="street" value="${addr.street}"/>
			</div>
			<div class="col-md-4">
				<label for="city">City</label>
				<input type="text" class="form-control" name="city" value="${addr.city}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="state">State</label>
				<input type="text" class="form-control" name="state" value="${addr.state}"/>
			</div>
			<div class="col-md-4">
				<label for="country">Country</label>
				<input type="text" class="form-control" name="country" value="${addr.country}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="postcode">Postal Code</label>
				<input type="number" class="form-control" name="postcode" value="${addr.postcode}"/>
			</div>
		</div>
		<input type="submit" class="btn btn-normal" value="Update Profile" style="margin-top:10px"/>
	</form>
</div>  
</body>
</html>