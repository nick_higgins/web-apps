<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="edu.unsw.comp9321.*, java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%-- <script src="<c:url value="/js/jquery-1.11.2.min.js" />"></script>
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<script src="<c:url value="/js/bootstrap.min.js" />"></script> --%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Wishlist</title>
<jsp:include page="Header.jsp"/>
</head>
<body>
<script>
function removeWishlist(itemId){
	
	 console.log("Remove " + itemId);
	$.ajax({
	     url:'wishlist?remove&itemId='+itemId,
	     success: function (data) {
	    	 $('div#'+itemId).remove();
	    	 console.log("Successfully removed: " + itemId);
	    	 if ($('div.row').length == 0)
	    		 $('#emptyWishlist').css('display', 'block');
	     },
	
		 error: function (data) {
			 if(data.status == 401){
				 $("#notify").html("You aren't logged in! Click <a href='login?next_page=wishlist'>here</a> to log in. <a class='close' data-dismiss='alert'>&times;</a>");
		  		 $("#notify").show();
			 } else {
				 $("#notify").html("There seems to be a problem with our server, please try again later.");
			 }
		 }
	});
	 
	 
}
</script>
<div id="emptyWishlist" class="jumbotron" style="display: none">
	<h1>Your wishlist is empty</h1>
</div>
<c:if test="${empty items}">
	<div id="emptyWishlist" class="jumbotron">
		<h1>Your wishlist is empty</h1>
	</div>
</c:if>

<c:forEach var="itemGroup" items="${items}">
<div class="row">
	<c:forEach var="item" items="${itemGroup}">
	  <div class="col-md-4" id="${item.itemId}">
	    <div class="thumbnail">
			<a href="view?itemId=${item.itemId}">
	    		<img src="${item.picture}" alt="Cannot find image">
	    	</a>
		    <div class="caption">
		        <h3>${item.title}</h3>
		        <p>${item.description}</p>
		        <p>
					<input type="button" onclick="removeWishlist('${item.itemId}')" value="Remove from Wishlist" class="btn btn-normal"/>
				</p>
		      </div>
		    </div>
		</div>
	</c:forEach>
  </div>
</c:forEach>
</body>
</html>