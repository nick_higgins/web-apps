<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="/css/global.css" />" rel="stylesheet">
<script src="<c:url value="/js/jquery-1.11.2.min.js" />"></script>
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<script src="<c:url value="/js/bootstrap.min.js" />"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin</title>
<jsp:include page="error.jsp"></jsp:include>
</head>
<body>
  <div id="loginContainer" style="width:400px; margin: 20px auto;">
    <form class="center" method="post" id="logInForm" action="login">
   	  <div id="logInFormContainer">
   	  	<input type="hidden" name="login" />
   	  	<input type="hidden" name="username" value="admin"/>
	   	<input style="width: 300px; margin: 10px auto;" class="form-control center style-input" type='password' placeholder="Password" name="password"/>
	    <input style="width: 300px; margin: 10px auto;" class="center btn btn-primary" type="submit" value="Log Into Admin"/>
      </div>
    </form>
  </div>
</body>
</html>