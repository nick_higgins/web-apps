<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="/css/global.css" />" rel="stylesheet">
<script src="<c:url value="/js/jquery-1.11.2.min.js" />"></script>
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet">
<script src="<c:url value="/js/bootstrap.min.js" />"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Log In</title>
<jsp:include page="Header.jsp"/>
</head>
<body>
  <div id="loginContainer" style="width:400px; margin: 20px auto;">
    <form class="center" method="post" id="logInForm" action="login">
   	  <div id="logInFormContainer">
   	  	<input type="hidden" name="login" />
   	  	<c:if test='${not empty param["next_page"]}'>
   	  		<input type="hidden" name="next_page" value='${param["next_page"]}' />
   	  	</c:if>
   	  	<c:if test='${not empty param["item"]}'>
   	  		<input type="hidden" name="item" value='${param["item"]}' />
   	  	</c:if>
        <input style="width: 300px; margin: 10px auto;" class="center style-input form-control" type='text' placeholder="Username" autofocus="autofocus" name="username"/>
	   	<input style="width: 300px; margin: 10px auto;" class="form-control center style-input" type='password' placeholder="Password" name="password"/>
	    <input style="width: 300px; margin: 10px auto;" class="center btn btn-primary" type="submit" value="Log In"/>
      </div>
    </form>
    <form class="center" id="logInForm" action="signup">
        <div style="border-bottom: 1px solid #999; margin: 30px 15px 20px;"></div>
        <div style="font-size: 9pt; font-weight: bold; margin: 10px 20px;" class="center">Not yet registered?</div>
        <input type="submit" style="width: 300px; margin: 10px auto;" class="center btn btn-danger" value="Sign Up"/>
	</form>    
  </div>
</body>
</html>