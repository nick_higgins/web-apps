<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import="edu.unsw.comp9321.AuctionItemHandler" %>   
<%@ page import="edu.unsw.comp9321.dto.ItemDTO" %>
<%@ page import="java.text.SimpleDateFormat" %>
<% AuctionItemHandler.handleViewItemFromRequest(request, session);%> 
   
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View Item</title>
<jsp:include page="Header.jsp"/>
</head>
<body>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-4">
		<img src='${item.picture}' style="height:240px; width:320px"/>	
	</div>
	<div class="col-md-6">
		<h2>${item.title}</h2>
		<p>${item.description}</p>
		<c:if test="${item.isInAuction == true}">
			<p><span class="label label-success">Auction Open</span></p>
		</c:if>
		<c:if test="${item.isInAuction == false}">
			<p><span class="label label-danger">Auction Closed</span></p>
		</c:if>
		<c:if test="${item.owner == sessionScope.username}">
			<p><strong>Reserve Price:</strong> ${item.reservePrice} ${item.biddingCurrency}</p>
		</c:if>
		<p><strong>Bidding Start:</strong> ${item.biddingStartPrice} ${item.biddingCurrency}</p>
		<p><strong>Bidding Increments:</strong> ${item.biddingIncrements} ${item.biddingCurrency}</p>
		<c:if test="${item.currentBiddingPrice > 0}">
			<p><strong>Current Bid: </strong>${item.currentBiddingPrice} ${item.biddingCurrency} by ${item.currentBidder}</p>
		</c:if>
		<c:if test="${item.currentBiddingPrice == 0}">
			<p><strong>No bids</strong></p>
		</c:if>
		<p><strong>End Time:</strong> <%= new SimpleDateFormat("hh:mm dd-MM-yyyy").format(((ItemDTO)request.getAttribute("item")).getEndTime()) %></p>
		<form action="view" method="post">
			<input type="hidden" name="itemId" value="${item.itemId}">
			<c:choose>
				<c:when test="${item_isSold == true}">
					<p><b> This item has been sold. You are viewing a snapshot of the item.</b></p>
				</c:when>
				<c:when test="${item_promptAcceptBelowReserve == true}">
					<input type="hidden" name="acceptBelowReserve" />
					<p><b> The highest bid is below reserve price.<br>
					Do you want to accept it?</b><br>
					<input type="submit" name="accept" value="Yes" class="btn btn-primary"/>
					<input type="submit" name="accept" value="No" class="btn btn-danger"/>
					</p>
				</c:when>
				<c:when test="${item_canBid == true}">					
					<c:if test="${sessionScope.username ne 'admin'}">
						<input type="hidden" name="bid" />
						<p><input type="number" class="form-control" name="bidPrice" value="<%= Math.max(((ItemDTO)request.getAttribute("item")).getCurrentBiddingPrice() + ((ItemDTO)request.getAttribute("item")).getBiddingIncrements(), ((ItemDTO)request.getAttribute("item")).getBiddingStartPrice()) %>" step="${item.biddingIncrements}"></p>
						<p><input type="submit" value="Bid" class="btn btn-primary"/></p>
					</c:if>
				</c:when>
				<c:when test="${item_canStartAuction == true}">
					<input type="hidden" name="start" />
					
					<div class="row">
						<div class="col-md-4">
							<label for="endTime">End Time</label>
							<input type="time" class="form-control" name="endTime" placeholder="hh:mm" value="${endTime}"/>
						</div>
						<div class="col-md-4">
						    <label for="endTime">End Date</label>
							<input type="date" class="form-control" name="endDate" placeholder="yyyy-MM-dd" value="${endDate}"/>
						</div>
					</div>&nbsp;
					<p><input type="submit" value="Start Auction" class="btn btn-primary"/></p>
				</c:when>
			</c:choose>	
		</form>
		<c:if test="${sessionScope.username eq 'admin' and item_canBid == true}">					
			<form action="admin" method="post">
				<input type="hidden" name="halt" />
				<input type="hidden" name="itemId" value="${item.itemId}"/>
				<input type="submit" value="Halt" class="btn btn-danger"/>				
			</form>
		</c:if>
		<c:if test="${sessionScope.username eq 'admin'}">
			<form action="admin" method="post" style="margin:10px 0px;">
				<input type="hidden" name="remove" />
				<input type="hidden" name="itemId" value="${item.itemId}"/>
				<input type="submit" value="Remove Item" class="btn btn-danger"/>				
			</form>		
		</c:if>
	</div>
</div>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-6">
		<h3>Postage Information</h3>
		<p><strong>Street:</strong> ${addr.street}</p>
		<p><strong>Post Code:</strong> ${addr.postcode}</p>
		<p><strong>City:</strong> ${addr.city}</p>
		<p><strong>State:</strong> ${addr.state}</p>
		<p><strong>Country:</strong> ${addr.country}</p>
	</div>
</div>
</body>
</html>