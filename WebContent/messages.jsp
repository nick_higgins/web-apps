<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="edu.unsw.comp9321.*, java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Messages</title>
<jsp:include page="Header.jsp"/>
<script>
function removeMessage(itemId){
	
	 console.log("Remove " + itemId);
	$.ajax({
	     url:'messages?remove&itemId='+itemId,
	     success: function (data) {
	    	 $('li#'+itemId).remove();
	    	 console.log("Successfully removed: " + itemId);
	    	 if ($('ul.messageList').length == 0)
	    		 $('#emptyMessages').css('display', 'block');

	     },
	
		 error: function (data) {
			 if(data.status == 401){
				 $("#notify").html("You aren't logged in! Click <a href='login?next_page=wishlist'>here</a> to log in. <a class='close' data-dismiss='alert'>&times;</a>");
		  		 $("#notify").show();
			 } else {
				 $("#notify").html("There seems to be a problem with our server, please try again later.");
			 }
		 }
	});
}
</script>
</head>
<body>
<div id="emptyMessages" class="jumbotron" style="display: none">
	<h1>You have no messages</h1>
</div>
<c:if test="${empty messages}">
	<div id="emptyMessages" class="jumbotron">
		<h1>You have no messages</h1>
	</div>
</c:if>
<ul class="list-group messageList">
	<c:forEach var="msg" items="${messages}">
		<li class="list-group-item" id="${msg.msgId}">
			<div class="row">
				<div class="col-md-11">
					<c:if test="${msg.unread == true}">
						<!-- Set background a different color -->
					</c:if>
					<a href="${pageContext.request.getContextPath()}/messages?view&msgId=${msg.msgId}">
						<h4>${msg.title}</h4>
			        	<p>${msg.message}</p>
			        	<p>${msg.generationTime}</p>
					</a>
				</div>
				<div class="col-md-1">
					<a href='#' class='close' onclick='removeMessage("${msg.msgId}")'>&times;</a>
				</div>
			</div>			
		</li>
	</c:forEach>
</ul>
</body>
</html>