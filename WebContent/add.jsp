<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="edu.unsw.comp9321.dto.Currency" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Item</title>
<jsp:include page="Header.jsp"/>
</head>
<body>
<div class="container">
	<form action='add' enctype="multipart/form-data" method="post">
		<input type="hidden" name="add" />
		<div class="row">
			<div class="col-md-4">
				<label for="title">Title</label>
				<input type="text" class="form-control" name="title" value="${title}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="picture">Picture</label>
				<input type="text" class="form-control" name="picture" value="${picture}"/>
			</div>
		  <div class="col-md-4">
		    <label for="uploadPicture">Upload Picture</label>
		    <input type="file" class="form-control" name="uploadPicture">
		    <p class="help-block">You can upload a picture or use a web link.</p>
		  </div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="category">Category</label>
				<input type="text" class="form-control" name="category" value="${category}"/>
			</div>
			<div class="col-md-4">
				<label for="description">Description</label>
				<input type="text" class="form-control" name="description" value="${description}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="reservePrice">Reserve Price</label>
				<input type="number" class="form-control" name="reservePrice" step="0.01" value="${reservePrice}"/>
			</div>
			<div class="col-md-4">
				<label for="biddingStartPrice">Bidding Start Price</label>
				<input type="number" class="form-control" name="biddingStartPrice" step="0.01"  value="${biddingStartPrice}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="biddingIncrements">Bidding Increments</label>
				<input type="number" class="form-control" name="biddingIncrements" step="0.01" value="${biddingIncrements}" />
			</div>
			<div class="col-md-4">
				<label for="currency">Currency</label>
				<select class="form-control" name ="currency">
					<c:set var="Currencies" value="<%=Currency.values()%>" />
					<c:forEach items="${Currencies}" var="curr">
						<option <c:if test="${curr eq 'AUD'}">selected='selected'</c:if> value="${curr}">${curr}</option>
					</c:forEach>
				</select>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="endTime">End Time</label>
				<input type="time" class="form-control" name="endTime" placeholder="hh:mm" value="${endTime}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
			    <label for="endTime">End Date</label>
				<input type="date" class="form-control" name="endDate" placeholder="yyyy-MM-dd" value="${endDate}"/>
			</div>
			<div class="col-md-4">
				<label for="streetAddress">Street Address</label>
				<input type="text" class="form-control" name="streetAddress" value="${streetAddress}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="city">City</label>
				<input type="text" class="form-control" name="city" value="${city}"/>
			</div>
			<div class="col-md-4">
				<label for="state">State</label>
				<input type="text" class="form-control" name="state" value="${state}"/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="country">Country</label>
				<input type="text" class="form-control" name="country" value="${country}"/>
			</div>
			<div class="col-md-4">
				<label for="postalCode">Postal Code</label>
				<input type="number" class="form-control" name="postalCode" value="${postalCode}"/>
			</div>
		</div>
		<input type="submit" class="btn btn-normal" value="Add item" style="margin-top:10px"/>
	</form>
</div>
</body>
</html>