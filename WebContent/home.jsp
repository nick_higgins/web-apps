<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="edu.unsw.comp9321.*, java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
<jsp:include page="Header.jsp"/>
<script>
function auctionItem(itemId){
		$.ajax({
		     url:'view/?item='+itemId,
		     success: function (data) {
		    	 $('btn#'+itemId).remove();
		    	 console.log("Successfully removed: " + itemId);
		     }
		});
}
</script>
</head>
<body>
<c:if test="${empty items}">
	<div id="emptyItems" class="jumbotron">
		<h1>You have no items</h1>
	</div>
</c:if>
<jsp:include page="error.jsp"/>
<c:forEach var="itemGroup" items="${items}">
<div class="row">
	<c:forEach var="item" items="${itemGroup}">
	  <div class="col-md-4">
	    <div class="thumbnail">
			<a href="view?itemId=${item.itemId}">
	    		<img src="${item.picture}" alt="Cannot find image">
	    	</a>
		    <div class="caption">
		        <h3>${item.title}</h3>
		        <p>${item.description}</p>
		        <p>
					<c:if test="${not item.isInAuction}">			
						<input type="button" onclick="auctionItem('${item.itemId}')" class="btn btn-normal" value="Auction item" id="${item.itemId}"/>				
					</c:if>
				</p>
		      </div>
		    </div>
		</div>
	</c:forEach>
  </div>
</c:forEach>
</body>
</html>