<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="edu.unsw.comp9321.*, java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<jsp:include page="Header.jsp"/>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Item Search</title>
</head>
<body>
<c:if test="${empty items}">
	<div id="emptyItems" class="jumbotron">
		<h1>No items found</h1>
	</div>
</c:if>
<c:forEach var="itemGroup" items="${items}">
<div class="row">
	<c:forEach var="item" items="${itemGroup}">
	  <div class="col-md-4">
	    <div class="thumbnail">
			<a href="view?itemId=${item.itemId}">
	    		<img src="${item.picture}" alt="Cannot find image">
	    	</a>
		    <div class="caption">
		        <h3>${item.title}</h3>
		        <p>${item.description}</p>
		        <p>
					<form action='wishlist'>
						<input type='hidden' name='add' />
						<input type='hidden' name='item' value='${item.itemId}'/>
						<input type='submit' value='Add To Wishlist' class="btn btn-default"/>
					</form>
				</p>
		      </div>
		    </div>
		</div>
	</c:forEach>
  </div>
</c:forEach>
</body>
</html>