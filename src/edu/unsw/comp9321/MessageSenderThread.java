package edu.unsw.comp9321;

import java.util.logging.Logger;

import edu.unsw.comp9321.dto.MessageDTO;

public class MessageSenderThread extends Thread {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private MessageDTO message;

    public MessageSenderThread(MessageDTO message) {
        super();
        this.message = message;
    }

    public void run() {
        if(!MessageHandler.sendMessage(this.message)){
            logger.severe("Failed to send message: " + this.message.getMsgId());
        }
    }
}