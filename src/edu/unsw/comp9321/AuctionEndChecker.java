package edu.unsw.comp9321;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Logger;

import edu.unsw.comp9321.dao.AddressDAO;
import edu.unsw.comp9321.dao.ItemDAO;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dto.AddressDTO;
import edu.unsw.comp9321.dto.ItemDTO;
import edu.unsw.comp9321.dto.UserDTO;

public class AuctionEndChecker implements Runnable 
{
    Logger logger = Logger.getLogger(this.getClass().getName());
    ItemDAO itemDao = null;

    @Override
    public void run()
    {
        // TODO Auto-generated method stub
        logger.info("AuctionEndChecker running...");
        itemDao = new ItemDAO();
        ArrayList<ItemDTO> itemlist;
        try 
        {
            itemlist = itemDao.findAll();
        } 
        catch (SQLException err) 
        {
            err.printStackTrace();
            logger.severe("itemDao.findAll() failed");
            return;
        }
        Iterator<ItemDTO> itemIter = itemlist.iterator();
        while (itemIter.hasNext())
        {
            ItemDTO item = itemIter.next();
            if (!item.getIsInAuction())
                continue;
            Timestamp timeNow = new Timestamp( (new Date()).getTime());
            if (!timeNow.before(item.getEndTime()))
                try {
                    processAuctionEnd(item);
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    logger.severe("processAuctionEnd threw an error.");
                    e.printStackTrace();
                }
        }
        itemDao.close();
        itemDao = null;
    }
    
    protected void processAuctionEnd(ItemDTO item) throws SQLException
    {
        logger.info("item: " + item.getTitle() + " -- auction ended");
        String title = null;
        String message = null;
        if (item.getCurrentBidder() == null)
        {
            // if no bid was placed at all
            // send email to owner
            title = "Your auction has ended";
            message = String.format("The auction for your item %s has ended.\n"
                    + "Unfortunately no bid was placed on the item and it did not sell.", item.getTitle());
            if (!MessageHandler.createMessage(item.getOwner(), title, message, false))
            {
                logger.severe("Trying to create auction end no bid message failed");
            }
            
        }
        else if (item.getCurrentBiddingPrice() < item.getReservePrice())
        {
            // reserve price was not reached.
            // send email to owner
            title = "Your auction has ended";
            message = String.format("The auction for your item %s has ended.\n"
                    + "Unfortunately the highest bid of %.2f %s did not reach "
                    + "your reserve price of %.2f %s. Please go to the item page to decide if "
                    + "you will accept or reject the highest bid.", 
                    item.getTitle(),
                    item.getCurrentBiddingPrice(), item.getBiddingCurrency().toString(),
                    item.getReservePrice(), item.getBiddingCurrency().toString());
            if (!MessageHandler.createMessage(item.getOwner(), title, message, false))
            {
                logger.severe("Trying to create auction end reserved not reached owner message failed");
            }
            // send email to highest bidder
            title = "An auction you had bid on has ended";
            message = String.format("The auction for item %s has ended.\n"
                    + "Unfortunately the your bid of %.2f %s did not reach "
                    + "the item owner's reserve price. The item owner will now decide "
                    + "if your bid will be accepted. You will be notified when the decision is made.", 
                    item.getTitle(),
                    item.getCurrentBiddingPrice(), item.getBiddingCurrency().toString());
            if (!MessageHandler.createMessage(item.getCurrentBidder(), title, message, false))
            {
                logger.severe("Trying to create auction end reserved not reached bidder message failed");
            }
        }
        else
        {
            // sold!
            UserDAO userDao = new UserDAO();
            AddressDAO addressDao = new AddressDAO();
            UserDTO owner = userDao.findUserByUsername(item.getOwner());
            UserDTO bidder = userDao.findUserByUsername(item.getCurrentBidder());
            AddressDTO address = addressDao.getAddress(bidder.getAddress());
            userDao.close();
            addressDao.close();
            String ownerCreditCard = owner.getCreditcard();
            ownerCreditCard = ownerCreditCard.substring(ownerCreditCard.length() - 4);
            String bidderCreditCard = bidder.getCreditcard();
            bidderCreditCard = bidderCreditCard.substring(bidderCreditCard.length() - 4);
            String bidderAddress = "";
            try{
                bidderAddress = String.format("\n%s,\n%s,\n%s,\n%d,\n%s\n", 
                address.getStreet() != null ? address.getStreet(): "",
                address.getCity() != null ? address.getCity(): "",
                address.getState() != null ? address.getState(): "",
                address.getPostcode(),
                address.getCountry() != null ? address.getCountry(): "");
            } catch (Exception err)
            {
                err.printStackTrace();
            }
            // send email to owner
            title = "SOLD! Your auction has ended";
            message = String.format("The auction for your item %s has ended.\n"
                    + "Congratulations, the highest bid on the item was %.2f %s from %s.\n"
                    + "Payment for the item has been transferred to your credit card number ending in %s.\n"
                    + "Please arrange to ship the item to the following address ASAP: %s", 
                    item.getTitle(),
                    item.getCurrentBiddingPrice(), item.getBiddingCurrency().toString(),
                    item.getCurrentBidder(), ownerCreditCard, bidderAddress);
            if (!MessageHandler.createMessage(item.getOwner(), title, message, false))
            {
                logger.severe("Trying to create auction end sold owner message failed");
            }
            // send email to highest bidder
            title = "You've won an auction!";
            message = String.format("The auction for item %s has ended.\n"
                    + "You were the highest bidder and have won the auction.\n"
                    + "%.2f %s has been billed from your credit card number ending in %s.\n"
                    + "The seller will ship the item to you ASAP.", 
                    item.getTitle(),
                    item.getCurrentBiddingPrice(), item.getBiddingCurrency().toString(),
                    bidderCreditCard);
            if (!MessageHandler.createMessage(item.getCurrentBidder(), title, message, false))
            {
                logger.severe("Trying to create auction end reserved not reached bidder message failed");
            }
            item.setIsSold(true);
        }
        item.setIsInAuction(false);
        itemDao.UpdateItem(item);
        logger.info("item: " + item.getTitle() + " -- auction end processing complete");
    }

}
