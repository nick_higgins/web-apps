package edu.unsw.comp9321.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.unsw.comp9321.AuctionItemHandler;
import edu.unsw.comp9321.dao.ItemDAO;

/**
 * Servlet implementation class Search
 */
@WebServlet("/search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    if(request.getParameter("advanced") == null){
	        simpleSearch(request, response);
	    } else {
	        //TODO advanced
	    }
	    
	    request.getRequestDispatcher("search.jsp").forward(request, response);
	}

	/**
     * @param request
     * @param response
     */
    private void simpleSearch(HttpServletRequest request,
                              HttpServletResponse response) {
        ItemDAO itemDAO = new ItemDAO();
        try {
            request.setAttribute("items", AuctionItemHandler.makeViewable(itemDAO.searchItems((String) request.getParameter("search"))));
        } catch (SQLException e) {
            request.setAttribute("error", "Our server is down... please come back later");
            e.printStackTrace();
        }finally {
            itemDAO.close();
        }
    }

    /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
