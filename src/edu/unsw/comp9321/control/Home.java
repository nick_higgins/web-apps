package edu.unsw.comp9321.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.unsw.comp9321.AuctionItemHandler;
import edu.unsw.comp9321.dao.ItemDAO;

/**
 * Servlet implementation class Home
 */
@WebServlet("/home")
public class Home extends ControlServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public Home() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        addItems(request);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    /**
     * @param request
     */
    private void addItems(HttpServletRequest request) {
        ItemDAO itemDAO = new ItemDAO();
        try {
            request.setAttribute("items",
                                 AuctionItemHandler.makeViewable(itemDAO.findSome(12)));
        } catch (SQLException e) {
            request.setAttribute("error",
                                 "Our server is down... please come back later");
            e.printStackTrace();
        } finally {
            itemDAO.close();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        doGet(request, response);
    }

}
