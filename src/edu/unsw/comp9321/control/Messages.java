package edu.unsw.comp9321.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.unsw.comp9321.MessageHandler;
import edu.unsw.comp9321.dao.GenericDAO.InsertionCheckException;
import edu.unsw.comp9321.dao.MessageDAO;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dto.MessageDTO;

/**
 * Servlet implementation class Messages
 */
@WebServlet("/messages")
public class Messages extends ControlServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Messages() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        if (request.getParameter("view") != null) {
            view(request, response);
        } else if (request.getParameter("unread") != null) {
            unread(request, response);
        } else if (request.getParameter("remove") != null){
        	remove(request, response);
        } else {
            viewAll(request, response);
        }
    }

    private void remove(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	if (request.getSession().getAttribute("username") == null)
    		response.sendError(401);

    	try {
			MessageHandler.removeMessage((String)request.getSession().getAttribute("username"), request.getParameter("itemId"));
    	} catch (IllegalArgumentException e) {
			e.printStackTrace();
			response.sendError(405);
		} catch (SQLException e) {
			e.printStackTrace();
			response.sendError(500, "Internal provlem with server");
		}    	
	}

	/**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void view(HttpServletRequest request, HttpServletResponse response) throws ServletException,
                                                                               IOException {
        if(request.getSession().getAttribute("username") == null){
            response.sendRedirect("login?next_page=messages?view&item=" + request.getParameter("item"));
            return;
        }
        MessageDAO msgDAO = new MessageDAO();
        try {
            MessageDTO msg = msgDAO.findMessageById(UUID.fromString(request.getParameter("msgId")));
            if (msg != null) {
                msg.setUnread(false);
                msgDAO.updateMessage(msg);
                List<MessageDTO> messages = new ArrayList<MessageDTO>(1);
                messages.add(msg);
                request.setAttribute("messages", messages);
            } else {
                request.setAttribute("error", "Message could not be found");
            }
        } catch (SQLException e1) {
            request.setAttribute("error",
                    "Our server seems to be having problems... please come back later");
            e1.printStackTrace();
        } catch (InsertionCheckException e){
            request.setAttribute("error",
                                 "Our server seems to be having problems... please come back later");
            e.printStackTrace();
        } finally {
            msgDAO.close();
        }

        RequestDispatcher rd = request.getRequestDispatcher("messages.jsp");
        rd.forward(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws IOException 
     */
    private void unread(HttpServletRequest request, HttpServletResponse response) throws IOException {
        UserDAO userDAO = new UserDAO();
        String username = request.getParameter("username");

        if (username != null && userDAO.exists(username)) {
            userDAO.close();

            MessageDAO msgDAO = new MessageDAO();
            MessageDTO msg = MessageHandler.checkUnread(username);
            
            if (msg != null) {
				response.getWriter().print(msg.getMessage());          		
                msg.setUnread(false);

                try {
                    msgDAO.updateMessage(msg);
                    msgDAO.close();
                } catch (InsertionCheckException e) {
                    e.printStackTrace();
                    msgDAO.close();
                	response.sendError(204);            	
                } catch (SQLException e) {
                    e.printStackTrace();                    
                    msgDAO.close();
                	response.sendError(204);            	
                }
               
            } else {
//            	logger.info("No unread messages");
                msgDAO.close();            	
            	response.sendError(204);            	
            }
        } else {
            userDAO.close();
            response.sendError(401);
        }
    }

    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void viewAll(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        HttpSession session = request.getSession();
        MessageDAO msgDAO = new MessageDAO();
        try {
            request.setAttribute("messages",
                                 msgDAO.findAllByUsername((String) session.getAttribute("username")));
        } catch (SQLException e) {
            request.setAttribute("error",
                                 "Our server is down... please come back later");
            e.printStackTrace();
        } finally {
            msgDAO.close();
        }

        RequestDispatcher rd = request.getRequestDispatcher("messages.jsp");
        rd.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        doGet(request, response);
    }

}
