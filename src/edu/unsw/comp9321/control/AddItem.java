package edu.unsw.comp9321.control;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import edu.unsw.comp9321.AddressHandler;
import edu.unsw.comp9321.AuctionItemHandler;
import edu.unsw.comp9321.dao.AddressDAO;
import edu.unsw.comp9321.dao.ItemDAO;
import edu.unsw.comp9321.dto.AddressDTO;
import edu.unsw.comp9321.dto.ItemDTO;

/**
 * Servlet implementation class AddItem
 */
@MultipartConfig
public class AddItem extends ControlServlet {
    protected class InvalidItem extends Exception {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        public InvalidItem(String message) {
            super(message);
        }
    }

    private static final long serialVersionUID = 1L;

    protected String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                s = s.substring(s.indexOf("=") + 2);
                return s.substring(s.lastIndexOf(File.separator) + 1,
                                   s.length() - 1);
            }
        }
        return "";
    }

    protected String uploadPicture(HttpServletRequest request) {
        //TODO check file type?
        Part part = null;
        try {
            part = request.getPart("uploadPicture");
        } catch (IllegalStateException e) {
            logger.severe(e.getMessage());
            return "";
        } catch (IOException e) {
            logger.severe(e.getMessage());
            return "";
        } catch (ServletException e) {
            logger.severe(e.getMessage());
            return "";
        }

        if (part == null) {
            try {
                for (Part p : request.getParts()) {
                    logger.info("Part name: " + p.getName());
                }
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ServletException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return "";
        }
        String fileName = "";
        fileName = extractFileName(part);
        logger.info("File Name: " + fileName);

        String savePath = request.getServletContext().getRealPath("")
                          + File.separator
                          + "img";

        File fileSaveDir = new File(savePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }

        try {
            part.write(savePath + File.separator + fileName);
        } catch (IOException e) {
            logger.severe(e.getMessage());
            return "";
        }
        return "img" + File.separator + fileName;
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddItem() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        if(request.getSession().getAttribute("username") == null){
            response.sendRedirect("login?next_page=add");
            return;
        }
        if (request.getParameter("add") != null) {
            logger.info("Adding new item");
            try {
                doAddItem(request, response);
            } catch (InvalidItem e) {
                request.setAttribute("error", e.getMessage());
                RequestDispatcher rd = request.getRequestDispatcher("add.jsp");
                rd.forward(request, response);
            }
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("add.jsp");
            rd.forward(request, response);
        }
    }

    /**
     * @param request
     * @param response
     * @throws InvalidItem
     * @throws IOException
     */
    protected void doAddItem(HttpServletRequest request,
                             HttpServletResponse response) throws InvalidItem,
                                                          IOException {
        String pic = uploadPicture(request);
        logger.info(pic);

        ItemDTO item = null;
        try {
            //Handlers should check everything is valid
            item = AuctionItemHandler.createItemFromRequest(request, pic);
            AddressDTO address = AddressHandler.createAddressFromRequest(request);

            AddressDAO addrdao = new AddressDAO();
            ItemDAO itemdao = new ItemDAO();
            try {
                addrdao.insertAddress(address);
                addrdao.close();

                item.setAddress(address.getUuid());

                itemdao.AddItem(item);
                itemdao.close();
                logger.info("Success");
            } catch (SQLException e) {
                addrdao.close();
                itemdao.close();
                //TODO check if address inserted and delete it

                logger.info("Failed to add item: " + e.getMessage());

                throw new InvalidItem("Failed to add item: " + e.getMessage());
            }

        } catch (IllegalArgumentException e) {
            //TODO add error or something;
            logger.info("Failed to add item: " + e.getMessage());

            throw new InvalidItem("Failed to add item: " + e.getMessage());
        }

        logger.info("Redirecting");
        response.sendRedirect("view?itemId=" + item.getItemId());
    }

}
