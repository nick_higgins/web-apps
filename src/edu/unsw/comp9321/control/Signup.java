package edu.unsw.comp9321.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.unsw.comp9321.MessageHandler;
import edu.unsw.comp9321.UserHandler;
import edu.unsw.comp9321.dao.GenericDAO.InsertionCheckException;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dto.UserDTO;

/**
 * Servlet implementation class Signup
 */
@WebServlet("/signup")
public class Signup extends ControlServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Signup() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        
        //TODO distinguish between SQL error and user already in db?
        
        if (request.getParameter("signup") != null) {
            try {
                doSignUp(request, response);
            } catch (IllegalArgumentException e){
                request.setAttribute("error", e.getMessage());

                request.getRequestDispatcher("signup.jsp").forward(request, response);
            } catch (InsertionCheckException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                request.getRequestDispatcher("signup.jsp").forward(request, response);
            } catch (SQLException e) {
                request.setAttribute("error", "Our server is reporting problems, please try again later");
                e.printStackTrace();

                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
        } else if(request.getParameter("confirm") != null){
            try {
                doConfirm(request, response);
            } catch (InsertionCheckException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
        } else if (request.getParameter("resend") != null){
            String url = MessageHandler.getBaseURL(request) + "/signup";
            String user = request.getParameter("user");
            MessageHandler.sendConfirmation(user,url + "?confirm&user="+ user);
            request.setAttribute("info", "Confirmation resent");
            request.getRequestDispatcher("login").forward(request, response);        	
        } else {
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    /**
     * @param request
     * @param response
     * @throws SQLException 
     * @throws InsertionCheckException 
     * @throws IOException 
     * @throws ServletException 
     */
    private void doSignUp(HttpServletRequest request,
                          HttpServletResponse response) throws InsertionCheckException, SQLException, IOException, ServletException {
        //TODO sanitize/validate input

        UserDTO user = null;

        try {
            user = UserHandler.createUser(request.getParameter("username"),
                                          request.getParameter("email"),
                                          request.getParameter("password"),
                                          request.getParameter("passwordCheck"));
        } catch (IllegalArgumentException e) {
            logger.info("Failed user creation: " + e.getMessage());
            throw e;
        }

        UserDAO dao = new UserDAO();
        if (user != null) {
            if(dao.exists(user.getUsername())){
                dao.close();
                throw new IllegalArgumentException("User already exists");
            }
            dao.addUser(user);
            dao.close();
            //System.err.println("Signed up " + request.getParameter("username"));
//            HttpSession session = request.getSession();
//            session.setAttribute("username", user.getUsername());

            //TODO User needs a confirmed field and message needs a title field
            String url = MessageHandler.getBaseURL(request) + "/signup";
            MessageHandler.sendConfirmation(user.getUsername(),
                                                url + "?confirm&user="
                                                    + user.getUsername());
            request.setAttribute("info", "You need to confirm your email");
            request.getRequestDispatcher("login").forward(request, response);
        } else {
            dao.close();
        }
    }
    

    /**
     * @param request
     * @param response
     * @throws InsertionCheckException 
     * @throws IOException
     */
    private void doConfirm(HttpServletRequest request,
                           HttpServletResponse response) throws InsertionCheckException, IOException {
        UserDAO userDAO = new UserDAO();
        try {
            UserDTO user = userDAO.findUserByUsername(request.getParameter("user"));
            //TODO check the user exists
            //TODO use some generated key to confirm users
            user.setConfirm(true);
            userDAO.updateUser(user);
            
            response.sendRedirect("profile");
        } catch (SQLException e) {
            request.setAttribute("error", "Our server is down... please come back later");              
            e.printStackTrace();
        } finally {
            userDAO.close();
        }
    }

}
