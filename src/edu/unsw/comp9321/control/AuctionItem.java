package edu.unsw.comp9321.control;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.UUID;

import javax.mail.Session;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.unsw.comp9321.AuctionItemHandler;
import edu.unsw.comp9321.MessageHandler;
import edu.unsw.comp9321.dao.AddressDAO;
import edu.unsw.comp9321.dao.ItemDAO;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dao.ItemDAO.ItemNotFoundException;
import edu.unsw.comp9321.dto.AddressDTO;
import edu.unsw.comp9321.dto.ItemDTO;
import edu.unsw.comp9321.dto.UserDTO;

/**
 * Servlet implementation class Auction
 */
@WebServlet("/view")
public class AuctionItem extends ControlServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuctionItem() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("error")!= null)
        {
            request.setAttribute("error", session.getAttribute("error"));
            session.removeAttribute("error");
        }
        if (session.getAttribute("success")!= null)
        {
            request.setAttribute("success", session.getAttribute("success"));
            session.removeAttribute("success");
        }
        if (session.getAttribute("info")!= null)
        {
            request.setAttribute("info", session.getAttribute("info"));
            session.removeAttribute("info");
        }
        viewItem(request, response);
        
        RequestDispatcher rd = request.getRequestDispatcher("view.jsp");
        rd.forward(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws IOException 
     * @throws ServletException 
     */
    private void viewItem(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        ItemDAO itemDAO = new ItemDAO();
        AddressDAO addrDAO = new AddressDAO();
        try {

            ItemDTO item = itemDAO.findItemById(UUID.fromString(request.getParameter("itemId")));
            AddressDTO addr = addrDAO.getAddress(item.getAddress());

            itemDAO.close();
            addrDAO.close();

            logger.info("Viewing item: " + item.getItemId());
            
            Timestamp end_time = item.getEndTime();
            SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat time_format = new SimpleDateFormat("hh:mm");
            
            request.setAttribute("endDate", date_format.format(end_time));
            request.setAttribute("endTime", time_format.format(end_time));

            request.setAttribute("item", item);
            request.setAttribute("addr", addr);

            HttpSession session = request.getSession();
            AuctionItemHandler.handleViewItemFromRequest(request, session);

        } catch (ItemNotFoundException e) {

            itemDAO.close();
            addrDAO.close();
            
            logger.info("Item not found: " + request.getParameter("itemId"));
            request.setAttribute("error", e.getMessage());
        } catch (SQLException e) {
            itemDAO.close();
            addrDAO.close();
            e.printStackTrace();
            
            request.setAttribute("error",
                                 "There seems to be something wrong with our server, please try again later");
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("username") == null){
            response.sendRedirect("login?next_page=view&itemId=" + request.getAttribute("itemId"));
            return;
        }
        
        if (request.getParameter("start") != null) {
            try {
                startAuction(request, response);
            } catch (ItemNotFoundException e) {
                logger.info("Item not found: " + request.getAttribute("itemId"));
                request.setAttribute("error", "Item not found");
            }
        } else if (request.getParameter("bid") != null){
            try {
                placeBid(request, response);
            } catch (ItemNotFoundException e) {
                request.setAttribute("error", "Item not found: " + request.getParameter("itemId"));
                request.getRequestDispatcher("home").forward(request, response);
            } catch (SQLException e) {
                e.printStackTrace();
                request.setAttribute("error", "There seems to be a problem with out server, please try again later");
                request.getRequestDispatcher("home").forward(request, response);
            }
        } else if (request.getParameter("acceptBelowReserve") != null){
            String decision = (String) request.getParameter("accept");
            if (decision.equals("Yes")) {
                acceptBelowReserve(request, response);
            } else {
                rejectBelowReserve(request, response);
                
            }
        }
    }

    /**
     * @param request
     * @throws IOException
     * @throws ServletException
     * @throws ItemNotFoundException 
     */
    private void startAuction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ItemNotFoundException {
        logger.info("Starting auction");
        //TODO check for item?
        //TODO finish this?
        HttpSession session = request.getSession();
        if (AuctionItemHandler.StartAuction(request, session)) {
        	boolean success = MessageHandler.createMessage((String) session.getAttribute("username"), 
        								"Auction started", 
        								"You have started the auction for your item.\n"
        								+ "You can view it here:\n"
        								+ MessageHandler.getBaseURL(request)
        								+ "view?item="+request.getParameter("itemId"), false);
        	if (!success)
        		request.setAttribute("error", "Failed to send message to user");
        } 
        AuctionItemHandler.handleViewItemFromRequest(request, session);
        
        String next = "view";
        
        if(request.getParameter("itemId") != null)
            next += "?itemId=" + request.getParameter("itemId");
        
        // temp use of session to pass err msg over redirect
        if (request.getAttribute("error") != null)
            session.setAttribute("error", request.getAttribute("error"));
        response.sendRedirect(next);
    }
    

    /**
     * @param request
     * @param response
     * @throws IOException 
     * @throws SQLException 
     * @throws ItemNotFoundException 
     */
    private void placeBid(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ItemNotFoundException, SQLException {
        //TODO check for stuff?
        HttpSession session = request.getSession();
        String previousBidder = AuctionItemHandler.HandleBid(request, session);
        ItemDAO itemDAO = new ItemDAO();
        ItemDTO item = itemDAO.findItemById(UUID.fromString(request.getParameter("itemId")));
        itemDAO.close();
        // HandleBid returns previousBidder as null if this bid is invalid.
        if ((item != null) && (previousBidder != null)){
	        MessageHandler.createMessage((String) session.getAttribute("username"), "Bid at Auction Site", 
	    			"You have bid " + item.getCurrentBiddingPrice() + " on item " + item.getTitle() + "\n"
	    					+ "You can view it here: " + MessageHandler.getBaseURL(request) + "/view?itemId=" + item.getItemId(), false);
	        
        	MessageHandler.createMessage(previousBidder, "Outbid at Auction Site",
        			"You have been outbid for item " + item.getTitle() + "\n"
	    					+ "You can view it here: " + MessageHandler.getBaseURL(request) + "/view?itemId=" + item.getItemId(), false);
        }
        AuctionItemHandler.handleViewItemFromRequest(request, session);
        
        // temp use of session to pass msg over redirect
        if (request.getAttribute("error") != null)
            session.setAttribute("error", request.getAttribute("error"));
        if (request.getAttribute("info") != null)
            session.setAttribute("info", request.getAttribute("info"));
        String next = "view?itemId=" + request.getParameter("itemId");
        try {
            response.sendRedirect(next);
        } catch (IOException err) {
            err.printStackTrace();
        }
    }

    private void acceptBelowReserve(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ItemDAO itemDAO = new ItemDAO();
        ItemDTO item = null;
        try {
            item = itemDAO.findItemById(UUID.fromString(request.getParameter("itemId")));
            item.setIsSold(true);
            itemDAO.UpdateItem(item);
        } catch (Exception err) {
            err.printStackTrace();
        }
        itemDAO.close();
        // Message Seller and Buyer
        String title = null;
        String message = null;
        UserDTO owner;
        UserDTO bidder;
        AddressDTO address;
        UserDAO userDao = new UserDAO();
        AddressDAO addressDao = new AddressDAO();
        try {
            owner = userDao.findUserByUsername(item.getOwner());
            bidder = userDao.findUserByUsername(item.getCurrentBidder());
            address = addressDao.getAddress(bidder.getAddress());
        } catch (Exception err)
        {
            userDao.close();
            addressDao.close();
            err.printStackTrace();
            return;
        }
        userDao.close();
        addressDao.close();
        String ownerCreditCard = owner.getCreditcard();
        ownerCreditCard = ownerCreditCard.substring(ownerCreditCard.length() - 4);
        String bidderCreditCard = bidder.getCreditcard();
        bidderCreditCard = bidderCreditCard.substring(bidderCreditCard.length() - 4);
        String bidderAddress = "";
        try {
            bidderAddress = String.format("\n%s\n%s\n%d\n%s\n", 
            address.getStreet() != null ? address.getStreet(): "",
            address.getState() != null ? address.getState(): "",
            address.getPostcode(),
            address.getCountry() != null ? address.getCountry(): "");
        } catch (Exception err) {
            err.printStackTrace();
        }
        // send email to owner
        title = "SOLD! Your item is sold";
        message = String.format("You have accepted the highest bid of %.2f %s on your item %s from %s.\n"
                + "Payment for the item has been transferred to your credit card number ending in %s.\n"
                + "Please arrange to ship the item to the following address ASAP: %s", 
                item.getCurrentBiddingPrice(), item.getBiddingCurrency().toString(), item.getTitle(),
                item.getCurrentBidder(), ownerCreditCard, bidderAddress);
        if (!MessageHandler.createMessage(item.getOwner(), title, message, false))
        {
            logger.severe("Trying to create accept below reserve owner message failed");
        }
        // send email to highest bidder
        title = "You've won an auction!";
        message = String.format("The owner of item %s has accepted your bid.\n"
                + "%.2f %s has been billed from your credit card number ending in %s.\n"
                + "The seller will ship the item to you ASAP.", 
                item.getTitle(),
                item.getCurrentBiddingPrice(), item.getBiddingCurrency().toString(),
                bidderCreditCard);
        if (!MessageHandler.createMessage(item.getCurrentBidder(), title, message, false))
        {
            logger.severe("Trying to create auction end reserved not reached bidder message failed");
        }
        
        // temp use of session to pass msg over redirect
        session.setAttribute("success", "SOLD! You have accepted the highest bid.");
        String next = "view?itemId=" + request.getParameter("itemId");
        try {
            response.sendRedirect(next);
        } catch (IOException err) {
            err.printStackTrace();
        }
    }

    private void rejectBelowReserve(HttpServletRequest request, HttpServletResponse response) {
        /* This function must set the current bid on the item to 0. */
        HttpSession session = request.getSession();
        ItemDAO itemDAO = new ItemDAO();
        ItemDTO item = null;
        String bidder = null;
        float bidPrice = 0;
        try {
            item = itemDAO.findItemById(UUID.fromString(request.getParameter("itemId")));
            bidder = item.getCurrentBidder();
            bidPrice = item.getCurrentBiddingPrice();
            item.setCurrentBiddingPrice(0);
            item.setCurrentBidder(null);
            itemDAO.UpdateItem(item);
        } catch (Exception err) {
            err.printStackTrace();
            return;
        }
        itemDAO.close();
        
        // email highest bidder (use variable 'bidder', not item.currentbidder
        String title = "The owner didn't accept your highest bid";
        String message = String.format("The auction for item %s has ended.\n"
                + "Unfortunately your bid of %.2f %s was below "
                + "the item owner's reserve price and the item was not sold.", 
                item.getTitle(),
                bidPrice, item.getBiddingCurrency().toString());
        if (!MessageHandler.createMessage(bidder, title, message, false))
        {
            logger.severe("Trying to create auction below reserve rejected message failed");
        }
        
        // temp use of session to pass msg over redirect
        session.setAttribute("success", "You have rejected the highest bid.");
        String next = "view?itemId=" + request.getParameter("itemId");
        try {
            response.sendRedirect(next);
        } catch (IOException err) {
            err.printStackTrace();
        }
    }
}
