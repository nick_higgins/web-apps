/**
 *
 */
package edu.unsw.comp9321.control;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import edu.unsw.comp9321.AuctionEndChecker;

/**
 * @author Timothy Masters
 */
public abstract class ControlServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    protected Logger logger = Logger.getLogger(this.getClass().getName());
    private static ScheduledExecutorService scheduler = null;;
    
    public void init(ServletConfig config) throws ServletException {
        if (scheduler == null)
        {
            scheduler = Executors.newSingleThreadScheduledExecutor();
            Runnable auctionChecker = new AuctionEndChecker();
            scheduler.scheduleAtFixedRate(auctionChecker, 5, 15, TimeUnit.SECONDS);
            logger.info("Auction End Check started");
        }
        super.init(config);
    }
}
