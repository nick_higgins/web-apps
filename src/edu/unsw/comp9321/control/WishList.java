package edu.unsw.comp9321.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.unsw.comp9321.AuctionItemHandler;
import edu.unsw.comp9321.WishlistHandler;
import edu.unsw.comp9321.dao.ItemDAO.ItemNotFoundException;

/**
 * Servlet implementation class WishList
 */
@WebServlet("/wishlist")
public class WishList extends ControlServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WishList() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("success") != null){
            request.setAttribute("success", session.getAttribute("success"));
            session.removeAttribute("success");
        }
        try {
            if (request.getParameter("add") != null) {
                if (session.getAttribute("username") == null) {
                    response.sendRedirect("login?next_page=wishlist?add&item="
                                          + request.getParameter("item"));
                    return;
                }
                add(request, response);
                response.sendRedirect("wishlist");
                return;
            } else if (request.getParameter("remove") != null) {
                if (session.getAttribute("username") == null) {
                    response.sendError(401);
                    return;
                }
                remove(request, response);
            } else {
                if (session.getAttribute("username") == null) {
                    response.sendRedirect("login?next_page=wishlist");
                    return;
                }
                view(request, response);
            }
        } catch (ItemNotFoundException e) {
            request.setAttribute("error", "Item not found");
        } catch (SQLException e) {
            request.setAttribute("error",
                                 "There seems to be a problem with our server, please try again later");
        }

        request.getRequestDispatcher("wishlist.jsp").forward(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws ItemNotFoundException
     */
    private void view(HttpServletRequest request, HttpServletResponse response) throws ItemNotFoundException {
        HttpSession session = request.getSession();
        try {
            request.setAttribute("items",
                                 AuctionItemHandler.makeViewable(WishlistHandler.getWishlistByUser((String) session.getAttribute("username"))));
        } catch (SQLException e) {
            request.setAttribute("error",
                                 "Our server is down... please come back later");
            e.printStackTrace();
        }
    }

    /**
     * @param request
     * @param response
     * @throws IllegalArgumentException 
     * @throws SQLException
     * @throws ItemNotFoundException
     */
    private void add(HttpServletRequest request, HttpServletResponse response) throws IllegalArgumentException, SQLException {
        HttpSession session = request.getSession();
        //TODO check for item
        WishlistHandler.addItem((String) session.getAttribute("username"),
                                UUID.fromString(request.getParameter("item")));
        session.setAttribute("success",
                             "Successfully added item to your wishlist.");

    }

    /**
     * @param request
     * @param response
     */
    private void remove(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        WishlistHandler.removeItem((String) session.getAttribute("username"),
                                   UUID.fromString(request.getParameter("itemId")));
    }

}
