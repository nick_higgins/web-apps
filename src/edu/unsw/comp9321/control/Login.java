package edu.unsw.comp9321.control;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.unsw.comp9321.MessageHandler;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dto.UserDTO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends ControlServlet {

    private static final long serialVersionUID = 1L;

    private class InvalidLogin extends Exception {

        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        public InvalidLogin(String message) {
            super(message);
        }
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        if(request.getParameter("login") != null){
            if(request.getSession().getAttribute("username") != null){
                logger.info("Already logged in");
            }
            
            logger.info("Attempting login");
            try {
                doLogin(request, response);
                logger.info("success");
                if(request.getParameter("next_page") != null){
                    String next = request.getParameter("next_page");
                    if(request.getParameter("itemId") != null)
                        next += "&itemId=" + request.getParameter("itemId");
                    response.sendRedirect(next);
                } else {
                    response.sendRedirect("myhome"); //TODO homepage
                }
                
            } catch (InvalidLogin e) {
                request.setAttribute("error", e.getMessage());
                logger.info("Error:" + e.getMessage());
                RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
                rd.forward(request, response);
            }
        } else if(request.getParameter("logout") != null) {
            request.getSession().removeAttribute("username");
            request.getSession().invalidate();
            response.sendRedirect("home");
        } else {
            if(request.getSession().getAttribute("username") != null){
                if(request.getParameter("next_page") != null){
                    String next = request.getParameter("next_page");
                    if(request.getParameter("itemId") != null)
                        next += "&itemId=" + request.getParameter("itemId");
                    response.sendRedirect(next);
                } else {
                    response.sendRedirect("myhome"); //TODO homepage
                }
                return;
            }
            RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
            rd.forward(request, response);
        }
    }

    protected void doLogin(HttpServletRequest request, HttpServletResponse response) throws InvalidLogin{
	    String username = request.getParameter("username");
        String password = request.getParameter("password");

        UserDAO dao = new UserDAO();
        try {
            UserDTO user = dao.findUserByUsername(username);
            dao.close();
            if (user != null) {
                //Check passwords
                String salt = user.getSalt();
                String hash = UserDAO.hashPassword(password, salt);

                if (hash.equals(user.getPassword())) {
                    if (user.getConfirm() && !user.getBanned()) {
                        HttpSession session = request.getSession();
                        session.setAttribute("username", user.getUsername());
                    } else if (user.getBanned()){
                        logger.info("Couldn't sign " + username + " in, they have been banned");
                        throw new InvalidLogin("You, " + username + ", have been banned");                    	
                    } else if (!user.getConfirm()){
                        logger.info("Couldn't sign " + username + " in, email not confirmed");
                        throw new InvalidLogin("You, " + username + ", have not confirmed their email, check your mailbox for our message or click for a resend:\n"
                        		+ MessageHandler.getBaseURL(request) + "/signup?resend&user="+username);
                    }
                } else {
                    logger.info("Couldn't sign user in, incorrect password: "
                                + username
                                + ", "
                                + password);
                    throw new InvalidLogin("Incorrect user password combination");
                }
            } else {
                logger.info("Couldn't sign user in, user not found:" + username);
                throw new InvalidLogin("Incorrect user password combination");
            }
        } catch (SQLException e) {
            dao.close();
            logger.info("Couldn't sign " + username + " in, SQL Error:" + e.getMessage());
            throw new InvalidLogin("There was an error logging you in. Please try again later");
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
}
