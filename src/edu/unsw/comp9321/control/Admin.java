package edu.unsw.comp9321.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.unsw.comp9321.AuctionItemHandler;
import edu.unsw.comp9321.UserHandler;
import edu.unsw.comp9321.dao.ItemDAO.ItemNotFoundException;

/**
 * Servlet implementation class Admin
 */
@WebServlet("/admin")
public class Admin extends ControlServlet implements Servlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Admin() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Halt has been moved to AuctionItem
	    //Admin is a normal user login from the normal login place
			
		if (request.getSession().getAttribute("username") != null && request.getSession().getAttribute("username").equals("admin")){
			if(request.getParameter("ban") != null){
		    	if (UserHandler.banUser(request))
		    		request.setAttribute("success", "The user, " + request.getParameter("username") + ", has been banned");
		    	
			    request.getRequestDispatcher("home").forward(request, response);
			} else if (request.getParameter("halt") != null) {
				try {
					AuctionItemHandler.haltAuction(request);
					request.setAttribute("success", "The auction has been halted");
					logger.info("Halted auction");
				} catch (ItemNotFoundException e) {
					request.setAttribute("error", e.getMessage());
					e.printStackTrace();
				}
			    request.getRequestDispatcher("view.jsp").forward(request, response);
			} else if (request.getParameter("remove") != null){
				try {
					AuctionItemHandler.removeItem(request);
					request.setAttribute("success", "The item has been removed");					
				    request.getRequestDispatcher("home").forward(request, response);
				} catch (ItemNotFoundException e){
					request.setAttribute("error", e.getMessage());
					e.printStackTrace();
				    request.getRequestDispatcher("view.jsp").forward(request, response);
				} catch (SQLException e){
				    request.getRequestDispatcher("view.jsp").forward(request, response);
				}
			} else {
				response.sendRedirect("admin.jsp");
			}
		} else {
			response.sendRedirect("admin.jsp");
		}
	}

}
