package edu.unsw.comp9321.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.unsw.comp9321.AuctionItemHandler;
import edu.unsw.comp9321.dao.ItemDAO;

/**
 * Servlet implementation class Home
 */
@WebServlet("/myhome")
public class MyHome extends ControlServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public MyHome() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
    	String username = (String) request.getSession().getAttribute("username");
    	if (username == null)
    		response.sendRedirect("home.jsp");
        addItems(request, username);
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    /**
     * @param request
     */
    private void addItems(HttpServletRequest request, String username) {
        ItemDAO itemDAO = new ItemDAO();
        try {
            request.setAttribute("items",
                                 AuctionItemHandler.makeViewable(itemDAO.findItemsByUser(username)));
        } catch (SQLException e) {
            request.setAttribute("error",
                                 "Our server is down... please come back later");
            e.printStackTrace();
        } finally {
            itemDAO.close();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        doGet(request, response);
    }

}
