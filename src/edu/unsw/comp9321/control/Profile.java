package edu.unsw.comp9321.control;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.unsw.comp9321.AddressHandler;
import edu.unsw.comp9321.UserHandler;
import edu.unsw.comp9321.dao.AddressDAO;
import edu.unsw.comp9321.dao.GenericDAO.InsertionCheckException;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dto.AddressDTO;
import edu.unsw.comp9321.dto.UserDTO;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/profile")
public class Profile extends ControlServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Profile() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("success") != null){
            request.setAttribute("success", session.getAttribute("success"));
            session.removeAttribute("success");
        }
        
        if(session.getAttribute("username") == null){
            response.sendRedirect("login?next_page=profile");
            return;
        }
        if (request.getParameter("update") != null) {
            logger.info("Updating user profile");
            try {
                doUpdate(request, response);
                logger.info("success");
                session.setAttribute("success", "Thank you for updating your profile");
                displayUser(request);
                return;
                
            } catch (IllegalArgumentException e){
            	e.printStackTrace();
                logger.info("fail");
                request.setAttribute("error", e.getMessage());
                displayUser(request);
            }
        } else {
            logger.info("Request user Update display");
        	displayUser(request);
        }
        
        RequestDispatcher rd = request.getRequestDispatcher("UpdateUser.jsp");
        rd.forward(request, response);
    }

    private void displayUser(HttpServletRequest request){
    	UserDTO user = UserHandler.getUser((String) request.getSession().getAttribute("username"));
    	AddressDTO addr = AddressHandler.getAddress(user.getAddress());
    	request.setAttribute("user", user);
    	if (addr != null)
    		request.setAttribute("addr", addr);
    }
    
    /**
     * 
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void doUpdate(HttpServletRequest request,
                          HttpServletResponse response) throws IOException,
                                                       ServletException {
        //TODO this is too long, it needs comments
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        if(username == null){
            throw new IllegalArgumentException("You're not logged in");
        }
        
        UserDAO userDAO = new UserDAO();
        AddressDAO addrDAO = new AddressDAO();

        UserDTO user = null;
        AddressDTO addr = null;

        try {
            user = userDAO.findUserByUsername(username);
            if (request.getParameter("addrId") != null)
            	addr = addrDAO.getAddress(UUID.fromString(request.getParameter("addrId")));
        } catch (SQLException e) {
            e.printStackTrace();

            request.setAttribute("error",
                                 "Our server is reporting that it has an error, please try again later");
            userDAO.close();
            addrDAO.close();

            return;
        }

        if (user == null) {
            userDAO.close();
            addrDAO.close();
            
            response.sendRedirect("signup?next_page=profile");
            return;
        } else if (addr == null) {
            addr = new AddressDTO();
            try {
                addrDAO.insertAddress(addr);
            } catch (SQLException err) {
                err.printStackTrace();
                request.setAttribute("error",
                        "Our server is reporting that it has an error, please try again later");
                userDAO.close();
                addrDAO.close();
                return;
            }
            user.setAddress(addr.getUuid());
        }

        if (!request.getParameter("password").isEmpty()
            || !request.getParameter("passwordCheck").isEmpty()) {
            if (!request.getParameter("password")
                        .equals(request.getParameter("passwordCheck"))) {
                userDAO.close();
                addrDAO.close();
                
                throw new IllegalArgumentException("Passwords don't match");
            } else {
                try {
                    user.setPassword(UserHandler.hashPassword(request.getParameter("password"),
                                                              user.getSalt()));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                    throw new IllegalArgumentException("Failed to save password");
                }
            }
        }

        user.setEmail(request.getParameter("email"));
        user.setCreditcard(request.getParameter("creditcard"));
        user.setFirstname(request.getParameter("firstname"));
        user.setLastname(request.getParameter("lastname"));
        user.setNickname(request.getParameter("nickname"));

        try {
            user.setYob(Integer.parseInt(request.getParameter("yob")));
        } catch (NumberFormatException e) {
            user.setYob(0);
        }

        addr.setState(request.getParameter("state"));
        addr.setStreet(request.getParameter("street"));
        addr.setCity(request.getParameter("city"));
        addr.setCountry(request.getParameter("country"));

        try {
            addr.setPostcode(Integer.parseInt(request.getParameter("postcode")));
        } catch (NumberFormatException e) {
            addr.setPostcode(0);
        }

        try {
            userDAO.updateUser(user);
            addrDAO.updateAddress(addr);
            
            userDAO.close();
            addrDAO.close();
            
            response.sendRedirect("profile");
            return;
        } catch (InsertionCheckException e) {

            userDAO.close();
            addrDAO.close();
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            userDAO.close();
            addrDAO.close();
            e.printStackTrace();
            request.setAttribute("error", "There was an error updating the database, please try again later");
        }
    }

}
