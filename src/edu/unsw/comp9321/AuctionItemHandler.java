package edu.unsw.comp9321;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import edu.unsw.comp9321.dao.ItemDAO;
import edu.unsw.comp9321.dao.ItemDAO.ItemNotFoundException;
import edu.unsw.comp9321.dao.PostgresConnFactory;
import edu.unsw.comp9321.dto.Currency;
import edu.unsw.comp9321.dto.ItemDTO;

public class AuctionItemHandler {
    static Logger logger = Logger.getLogger("AuctionItemHandler");

    public AuctionItemHandler() {}

    public static ItemDTO createItemFromRequest(HttpServletRequest request,
                                                String pic) throws IllegalArgumentException {
        ItemDTO item = new ItemDTO();

        if(request.getParameter("title").length() > 128)
            throw new IllegalArgumentException("Title is too long (maximum 128 characters)");
        //TODO ather limits
        
        if (request.getParameter("title").isEmpty())
            throw new IllegalArgumentException("Missing Title");
        else {
            item.setTitle(request.getParameter("title"));
        }

        if (request.getParameter("category").isEmpty())
            throw new IllegalArgumentException("Missing Category");
        else
            item.setCategory(request.getParameter("category"));

        if (pic.isEmpty()) {
            if (request.getParameter("picture").isEmpty())
                throw new IllegalArgumentException("Missing Picture");
            else
                item.setPicture(request.getParameter("picture"));
        } else {
            item.setPicture(pic);
        }

        if (request.getParameter("description").isEmpty()
            || request.getParameter("description").length() > 1000)
            throw new IllegalArgumentException("Missing Description");
        else
            item.setDescription(request.getParameter("description"));

        try {
            item.setReservePrice(Float.parseFloat(request.getParameter("reservePrice")));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid Reserve Price");
        }
        try {
            item.setBiddingStartPrice(Float.parseFloat(request.getParameter("biddingStartPrice")));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid Bidding Start Price");
        }

        try {
            item.setBiddingIncrements(Float.parseFloat(request.getParameter("biddingIncrements")));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid Bidding Increments");
        }
        
        //Default end time is 10 minutes from now
        long endTime = System.currentTimeMillis() + 10 * 60 * 1000;
        Timestamp ts = new Timestamp(endTime);
        item.setEndTime(ts);
        /*String datetime = ((String) request.getParameter("endDate"))
                          + "T"
                          + request.getParameter("endTime");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        try {
            long time = df.parse(datetime).getTime();
            Timestamp ts = new Timestamp(time);
            item.setEndTime(ts);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid End Time. It needs to be in the format yyyy-MM-DDTHH:mm:ss.S");
        }

        if (item.getEndTime().before(new Date())) {
            logger.info(new Date().toString()
                        + ":"
                        + item.getEndTime().toString());
            throw new IllegalArgumentException("Invalid End Time. End time must be in the future");
        }*/

        //item.setItemId(UUID.randomUUID());

        Currency currency = Currency.parse(request.getParameter("currency"));
        if (currency != null)
            item.setBiddingCurrency(currency);
        else
            throw new IllegalArgumentException("Invalid Currency. It needs to be either AUD, GBP, USD or EUR");

        HttpSession session = request.getSession();
        item.setOwner((String) session.getAttribute("username"));

        //logger.info("Added bean -> " + item.toString());
        return item;
    }

    public static void handleViewItemFromRequest(HttpServletRequest request,
                                                 HttpSession session) {
        /* Handles additional features on the item view page depending on session info.
         * view.jsp should check for the following attributes in request:
         *     item_canBid
         *     item_canStartAuction
         * 
         */
        ItemDTO item = (ItemDTO) request.getAttribute("item");
        String currentUser = (String) session.getAttribute("username");
        request.removeAttribute("item_canStartAuction");
        request.removeAttribute("item_canBid");
        request.removeAttribute("item_isSold");
        request.removeAttribute("item_promptAcceptBelowReserve");
        request.removeAttribute("item_afterEndDate");
        if (currentUser == null) {
            return;
        }
        // check if item is sold.
        if (item.getIsSold()) {
            request.setAttribute("item_isSold", true);
            return;
        }
        if (currentUser.equals(item.getOwner())) {
            if (item.getCurrentBiddingPrice() > 0 && !item.getIsInAuction() &&
                item.getCurrentBiddingPrice() < item.getReservePrice()){
                /* Condition for user decision:
                 * 1. item not sold
                 * 2. current user is owner
                 * 3. item is not current in auction
                 * 4. 0 < current bid < reserve price.
                 */
                // Must reset currentBiddingPrice to zero if owner rejects the bid.
                request.setAttribute("item_promptAcceptBelowReserve", true);
            }
            else if (!item.getIsInAuction()) 
            {
                /* Condition for start auction:
                 * 1. item not sold
                 * 2. current user is owner
                 * 3. item not currently in auction
                 */
                request.setAttribute("item_canStartAuction", true);
            }
            
            if(item.getEndTime().before(new Date())){
                request.setAttribute("item_afterEndDate", true);
            }
        } else {
            if (item.getIsInAuction())
                request.setAttribute("item_canBid", true);
        }
    }

    public static String HandleBid(HttpServletRequest request,
                                   HttpSession session) throws ItemNotFoundException,
                                                       SQLException {
        // returns null if bid is invalid, or name of previous highest bidder if bid is registered.
        ItemDAO itemDao = new ItemDAO();
        ItemDTO item = null;
        String itemId = (String) request.getParameter("itemId");
        String username = (String) session.getAttribute("username");
        if (itemId == null) {
            request.setAttribute("error", "No item to bid on.");
            return null;
        }
        try {
            item = itemDao.findItemById(UUID.fromString(itemId));
        } catch (SQLException err) {
            // TODO Auto-generated catch block
            err.printStackTrace();
            itemDao.close();
            return null;
        }
        request.setAttribute("item", item);
        if (username == null) {
            request.setAttribute("error", "You can not bid when not logged in.");
            itemDao.close();
            return null;
        }
        // check item is in auction
        if (!item.getIsInAuction()) {
            request.setAttribute("error",
                                 "This item is not currently in auction.");
            itemDao.close();
            return null;
        }
        // check session user can bid
        if (username.equals(item.getOwner())) {
            request.setAttribute("info", "You cannot bid on your own item.");
            itemDao.close();
            return null;
        } else if (username.equals(item.getCurrentBidder())) {
            request.setAttribute("info", "You cannot outbid yourself!");
            itemDao.close();
            return null;
        }
        // check bidding value is valid
        float bidPrice = Float.parseFloat(request.getParameter("bidPrice"));
        boolean isFirstBid = item.getCurrentBiddingPrice() == 0;
        if (isFirstBid && bidPrice < item.getBiddingStartPrice()) {
            String minPrice = ""
                              + (item.getBiddingStartPrice())
                              + " "
                              + item.getBiddingCurrency().toString();
            request.setAttribute("info", "Please set your bid to at least "
                                         + minPrice);
            itemDao.close();
            return null;
        }
        if (!isFirstBid
            && bidPrice < (item.getCurrentBiddingPrice() + item.getBiddingIncrements())) {
            String minPrice = ""
                              + (item.getCurrentBiddingPrice() + item.getBiddingIncrements())
                              + " "
                              + item.getBiddingCurrency().toString();
            request.setAttribute("info", "Please set your bid to at least "
                                         + minPrice);
            itemDao.close();
            return null;
        }
        String previousBidder = item.getCurrentBidder();
        item.setCurrentBiddingPrice(bidPrice);
        item.setCurrentBidder(username);
        itemDao.UpdateItem(item);
        request.setAttribute("success", "Yout bid has been accepted");
        itemDao.close();
        return previousBidder;
    }

    public static boolean StartAuction(HttpServletRequest request,
                                       HttpSession session) throws ItemNotFoundException {
        ItemDAO itemDao = new ItemDAO();
        ItemDTO item = null;
        String itemId = (String) request.getParameter("itemId");
        String username = (String) session.getAttribute("username");
        if (itemId == null) {

            itemDao.close();
            request.setAttribute("error", "No item specified.");
            logger.severe("itemId is null");
            return false;
        }
        try {
            item = itemDao.findItemById(UUID.fromString(itemId));
        } catch (SQLException err) {

            itemDao.close();
            err.printStackTrace();
            return false;
        }
        request.setAttribute("item", item);
        if (username == null) {

            itemDao.close();
            logger.severe("session username is null");
            request.setAttribute("error", "You can are not logged in!");
            return false;
        }
        // check item is in auction
        if (item.getIsInAuction()) {

            itemDao.close();
            logger.warning("item already in auction");
            request.setAttribute("info",
                                 "This item is already currently in auction.");
            return false;
        }
        if (!username.equals(item.getOwner())) {

            itemDao.close();
            logger.severe("item owner is not currently logged in user.");
            request.setAttribute("error", "This item doesn't belong to you!");
            return false;
        }
        
        if(request.getParameter("endDate") != null && request.getParameter("endTime") != null){
            String datetime = ((String) request.getParameter("endDate"))
                            + "T"
                            + request.getParameter("endTime");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            try {
              long time = df.parse(datetime).getTime();
              Timestamp ts = new Timestamp(time);
              item.setEndTime(ts);
            } catch (ParseException e) {
              throw new IllegalArgumentException("Invalid End Time. It needs to be in the format yyyy-MM-DDTHH:mm:ss.S");
            }
        }
        
        long duration = item.getEndTime().getTime()
                        - System.currentTimeMillis();
        if (duration < (60 * 3 * 1000)) {

            itemDao.close();
            logger.severe("auction too short");
            request.setAttribute("error",
                                 "Auction duration must be at least 3 minutes.");
            return false;
        }
        if (duration > (60 * 60 * 1000)) {

            itemDao.close();
            logger.severe("auction too long");
            request.setAttribute("error",
                                 "Auction duration must be at most 60 minutes.");
            return false;
        }
        item.setIsInAuction(true);
        item.setCurrentBiddingPrice(0);
        try {
            itemDao.UpdateItem(item);

            itemDao.close();
            request.setAttribute("success",
                                 "This item is now up for auction.");
        } catch (SQLException e) {

            itemDao.close();
            e.printStackTrace();
        }
        return true;
    }

    public static void haltAuction(HttpServletRequest request) throws ItemNotFoundException {
        ItemDAO itemDao = new ItemDAO();
        ItemDTO item = null;
        String itemId = (String) request.getParameter("itemId");
        if (itemId == null) {
            request.setAttribute("error", "No item specified.");
            logger.severe("itemId is null");
            itemDao.close();
            return;
        }
        try {
            item = itemDao.findItemById(UUID.fromString(itemId));
        } catch (SQLException err) {
            err.printStackTrace();
            itemDao.close();
            request.setAttribute("error", "Database error, failed to halt");
            return;
        }
        item.setIsInAuction(false);
        try {
            itemDao.UpdateItem(item);
            request.setAttribute("success", "Stopped Auction");
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("error", "Failed to halt auction");
        }

        itemDao.close();

        request.setAttribute("item", item);
    }

    public static ArrayList<ArrayList<ItemDTO>> makeViewable(ArrayList<ItemDTO> items) {
        logger.info("Making viewable");
        ArrayList<ArrayList<ItemDTO>> viewable = new ArrayList<ArrayList<ItemDTO>>();
        int count = 0;
        int rows = 0;
        for (ItemDTO item : items) {
            if (count == 0)
                viewable.add(new ArrayList<ItemDTO>());
            viewable.get(rows).add(item);
            count++;
            if (count == 3) {
                count = 0;
                rows++;
            }
        }
        return viewable;
    }

	public static void removeItem(HttpServletRequest request) throws ItemNotFoundException, SQLException {
        ItemDAO itemDao = new ItemDAO();
        String itemId = (String) request.getParameter("itemId");
        if (itemId == null) {
            request.setAttribute("error", "No item specified.");
            logger.severe("itemId is null");
            itemDao.close();
            return;
        }
        try {
        	WishlistHandler.removeItems(itemId);
            itemDao.deleteItemById(UUID.fromString(itemId));
            itemDao.close();
        } catch (SQLException err) {
            err.printStackTrace();
            itemDao.close();
            request.setAttribute("error", "Database error, failed to remove");
            throw err;
        }
	}
}
