package edu.unsw.comp9321;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Logger;

import edu.unsw.comp9321.dao.ItemDAO;
import edu.unsw.comp9321.dao.ItemDAO.ItemNotFoundException;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dao.WishlistDAO;
import edu.unsw.comp9321.dto.ItemDTO;
import edu.unsw.comp9321.dto.WishlistDTO;

public class WishlistHandler {
	Logger logger = Logger.getLogger(this.getClass().getName());

	public static ArrayList<ItemDTO> getWishlistByUser(String username) throws SQLException, ItemNotFoundException {
		WishlistDAO wishlistDAO = new WishlistDAO();
		ItemDAO itemDAO = new ItemDAO();
		
		ArrayList<WishlistDTO> wishlist = wishlistDAO.getWishlistByUser(username);
		ArrayList<ItemDTO> items = new ArrayList<ItemDTO>();
		for (WishlistDTO item : wishlist){
            try {
                items.add(itemDAO.findItemById(item.getItemID()));
            } catch (SQLException e) {

                wishlistDAO.close();
                itemDAO.close();
                
                throw e;
            } catch (ItemNotFoundException e) {

                wishlistDAO.close();
                itemDAO.close();
                
                throw e;
            }
		}
				
		
		wishlistDAO.close();
		itemDAO.close();
		return items;
	}
	
	public static void addItem(String username, UUID item) throws IllegalArgumentException, SQLException {
		WishlistDAO wishlistDAO = new WishlistDAO();
		ItemDAO itemDAO = new ItemDAO();
		UserDAO userDAO = new UserDAO();
		
		if (!userDAO.exists(username))
			throw new IllegalArgumentException("User " + username + " does not exist");
		if (!itemDAO.exists(item))
			throw new IllegalArgumentException("Item does not exist");
		
		
		
		WishlistDTO wishlist = new WishlistDTO();
		wishlist.setItemID(item);
		wishlist.setUsername(username);
		
		if (wishlistDAO.exists(wishlist))
			throw new IllegalArgumentException("Item is already in your wishlist");
			
		itemDAO.close();
		userDAO.close();
		try {
            wishlistDAO.addWishlist(wishlist);
            wishlistDAO.close();
        } catch (SQLException e) {
            wishlistDAO.close();
            throw e;
        }
	}
	
	public static void removeItem(String username, UUID item) {
		WishlistDAO wishlistDAO = new WishlistDAO();
		WishlistDTO wishlist = new WishlistDTO();
		wishlist.setItemID(item);
		wishlist.setUsername(username);
		try {
            wishlistDAO.removeWishlist(wishlist);
            wishlistDAO.close();
        } catch (SQLException e) {
            wishlistDAO.close();
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}

	public static void removeItems(String itemId) throws SQLException {
		WishlistDAO wishlistDAO = new WishlistDAO();
		try {
            wishlistDAO.removeAllItemsWishlist(itemId);
            wishlistDAO.close();
        } catch (SQLException e) {
            wishlistDAO.close();
            throw e;
        }
	}	
}
