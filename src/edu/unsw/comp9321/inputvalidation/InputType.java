/**
 * @author Timothy Masters
 */
package edu.unsw.comp9321.inputvalidation;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum InputType {
    BUTTON("button"),
    CHECKBOX("checkbox"),
    COLOR("color"),
    DATE("date"),
    DATETIME("datetime"),
    DATETIME_LOCAL("datetime-local"),
    EMAIL("email"),
    FILE("file"),
    HIDDEN("hidden"),
    IMAGE("image"),
    MONTH("month"),
    NUMBER("number"),
    PASSWORD("password"),
    RADIO("radio"),
    RANGE("range"),
    RESET("reset"),
    SEARCH("search"),
    SUBMIT("submit"),
    TEL("tel"),
    TEXT("text"),
    TEXTAREA(null),
    TIME("time"),
    URL("url"),
    WEEK("week");

    private final String         print;

    private static final String  COLOR_FORMAT_STD     = "#[a-fA-F0-9]{6}";
    private static final String  COLOR_FORMAT_CSS     = "#[a-fA-F0-9]{3}";
    private static final String  DATE_FORMAT_PATTERN  = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
    private static final String  MONTH_FORMAT_PATTERN = "[0-9]{4}-[0-9]{2}";
    private static final String  TIME_FORMAT_PATTERN  = "[0-9]{2}:[0-9]{2}(:[0-9]{2}(\\.[0-9])*)*";
    private static final String  EMAIL_FORMAT_PATTERN = "[a-zA-Z0-9!#\\$%&'\\*\\+-/=?\\^_`\\{\\|\\}~\\.]+@[a-zA-Z0-9\\-]+(\\.[a-zA-Z0-9\\-]+)*";
    private static final String  ESCAPE_CHARS_PATTERN = "(\\W)";

    private static final Pattern escapechars          = Pattern.compile(ESCAPE_CHARS_PATTERN);
    private static final Pattern colorpattern_std     = Pattern.compile(COLOR_FORMAT_STD);
    private static final Pattern colorpattern_css     = Pattern.compile(COLOR_FORMAT_CSS);
    private static final Pattern datepattern          = Pattern.compile(DATE_FORMAT_PATTERN);
    private static final Pattern monthpattern         = Pattern.compile(MONTH_FORMAT_PATTERN);
    private static final Pattern timepattern          = Pattern.compile(TIME_FORMAT_PATTERN);
    private static final Pattern emailpattern         = Pattern.compile(EMAIL_FORMAT_PATTERN);

    private static final String  ALLOWED_VALUES_MSG   = "Input not in allowed values";
    private static final String  LENGTH_MIN_MSG       = "Input length less than minimum";
    private static final String  LENGTH_MAX_MSG       = "Input length greater than maximum";
    private static final String  FORMAT_MSG           = "Input doesn't match specified format";
    private static final String  INVALID_MSG          = "Input is an invalid value";

    public static final String   ESCAPING_WARNING     = "Needs sanitation";

    public static final String addSlashes(String s) {
        return (s != null) ? escapechars.matcher(s).replaceAll("\\\\$1"): null;
    }
    
    public static final String removeSlashes(String s) {
        String ret = s;
        if (s == null)
            return null;
        Pattern p = Pattern.compile("((\\\\\\W))");
        Matcher m = p.matcher(s);
        
        while(m.find()){
            String match = m.group(1);
            ret = ret.replace(match, match.substring(1));
        }
        
        return ret;
    }

    private InputType(String print) {
        this.print = print;
    }

    public String toString() {
        return this.print;
    }

    public Validation validate(String string,
                               List<String> allowedValues,
                               String min,
                               String max) {
        Error e = new UnimplementedError();
        e.printStackTrace();

        // TODO

        switch (this) {
        default:
            return validate(string, allowedValues);
        }
    }

    public Validation validate(String string, List<String> allowedValues) {
        // Switch
        switch (this) {
        case HIDDEN:
        case TEXT:
        case SEARCH:
        case TEXTAREA:
        case PASSWORD:
        case RADIO: // Defer to allowed values
        case CHECKBOX: // Defer to allowed values
        case BUTTON: // Defer to allowed
            return validateString(string, allowedValues); //TODO some of these might have different constraints
        case SUBMIT:
        case FILE: // TODO validate file input?
        case IMAGE: // TODO validate image input?
        case RESET: // TODO validate reset?
            return new Validation();
        case COLOR:
            return validateColor(string, allowedValues);
        case DATE:
            return validateDate(string, allowedValues);
        case DATETIME:
        case DATETIME_LOCAL:
            return validateDateTime(string, allowedValues);
        case EMAIL:
            return validateEmail(string, allowedValues);
        case NUMBER:
        case RANGE:
            return validateNumber(string, allowedValues);
        case MONTH:
            return validateMonth(string, allowedValues);
        case WEEK:
            return validateWeek(string, allowedValues);
        case TEL:
            return validateTel(string, allowedValues);
        case TIME:
            return validateTime(string, allowedValues);
        case URL:
            return validateURL(string, allowedValues);
        default:
            return null;
        }
    }

    /**
     * Validates a string as a color value
     * 
     * @param string
     *            the input to validate
     * @param allowedValues
     *            the allowed values, set to null to allow anything
     * @return A Validation for the given input<br>
     *         An error will be given if the color is not in the format of a #
     *         character followed by three or six characters in the range 0�9,
     *         a�f, and A�F<br>
     *         If the input only contains three numbers a warning will be added
     *         indicating that the value conforms to css standard but not html5<br>
     *         An error will also be added if allowedValues is not null and the
     *         input is not in the allowedValues list
     */
    private Validation validateColor(String string, List<String> allowedValues) {
        Validation validation = new Validation();

        // Check format
        if (!colorpattern_std.matcher(string).matches()) {
            if (!colorpattern_css.matcher(string).matches()) {
                validation.addError(FORMAT_MSG + ": html5 or css colors");
            } else {
                validation.addWarning(FORMAT_MSG + ": html5 color");
            }
        }

        if (allowedValues != null && !allowedValues.contains(string)) {
            validation.addError(ALLOWED_VALUES_MSG);
        }

        return validation;
    }

    /**
     * Validates a string as a date
     * 
     * @param string
     *            the input string to validate
     * @param allowedValues
     *            a list of allowed values, set to null to allow anything
     * @return A Validation for the given input<br>
     *         An error will be given if the input doesn't match the format
     *         yyyy-MM-dd exactly or isn't a valid date according to RFC3339
     *         with the additional qualification that the year is greater than
     *         zero (according to w3 standards as of 2015)<br>
     *         An error will be given if the allowedValues list is not null and
     *         doesn't contain allowedValues
     */
    private Validation validateDate(String string, List<String> allowedValues) {
        Validation validation = new Validation();

        if (!datepattern.matcher(string).matches()) {
            validation.addError(FORMAT_MSG + ". Date format: yyyy-mm-dd");
        } else {
            int year = Integer.parseInt(string.substring(0, 4));
            int month = Integer.parseInt(string.substring(5, 7));
            int day = Integer.parseInt(string.substring(8, 10));

            if (year <= 0)
                validation.addError(INVALID_MSG + ". Date requires a year > 0");
            if (month <= 0 || month > 12)
                validation.addError(INVALID_MSG + ". Date requires valid month");
            else if (day <= 0 || day > maxDay(month, year))
                validation.addError(INVALID_MSG + ". Date requires valid day");
        }

        return validation;
    }

    private int maxDay(int month, int year) {
        final int[] max_days = {
                0,
                31,
                28,
                31,
                30,
                31,
                30,
                31,
                31,
                30,
                31,
                30,
                31 };
        if (month == 2 && isleap(year)) {
            return max_days[month] + 1;
        }
        return max_days[month];
    }

    private boolean isleap(int year) {
        return (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
    }

    /**
     * Validate a string as a time
     * 
     * @param string
     *            //TODO comments
     * @param allowedValues
     * @return
     */
    private Validation validateTime(String string, List<String> allowedValues) {
        Validation validation = new Validation();
        if (!timepattern.matcher(string).matches()) {
            validation.addError(FORMAT_MSG
                                + ". Time format: hh:mm:ss with optional 1 decimal place for seconds");
        } else {
            int m_index = string.indexOf(':');
            int s_index = string.indexOf(':', m_index + 1);
            // Format requires this be a number so we don't have to check for
            // numberformat exception
            int hours = Integer.parseInt(string.substring(0, m_index));
            int minutes = Integer.parseInt(string.substring(m_index + 1,
                                                            s_index));
            float seconds = Float.parseFloat(string.substring(s_index + 1));
            if (hours > 23) {
                validation.addError(INVALID_MSG + ". Hours must be < 24");
            }
            if (minutes > 59) {
                validation.addError(INVALID_MSG + ". Minutes must be < 60");
            }
            if (seconds >= 60) {
                validation.addError(INVALID_MSG + ". Seconds must be < 60");
            }
        }
        return validation;
    }

    /**
     * Validate a string as a datetime
     * 
     * @param string
     *            the input to validate
     * @param allowedValues
     *            the allowed values, set to null to allow anything
     * @return a validation for the string<br>
     *         Errors will be generated if the format doesn't match the format
     *         given by w3.org as of 2015
     */
    private Validation validateDateTime(String string,
                                        List<String> allowedValues) {
        final String FORMAT = ". DateTime: yyyy-MM-dd followed by 'T' followed by hh:mm:ss with and optional 1 decimal place for the seconds eg. 2000-01-01T12:30:01.1";
        Validation v = new Validation();
        String date;
        String time;
        int t_idx = string.toUpperCase().indexOf("T");
        if (t_idx > 0) {
            date = string.substring(0, t_idx);
            time = string.substring(t_idx + 1);

            // Validate date
            Validation date_v = validateDate(date, null);
            for (String s : date_v.warnings()) {
                v.addWarning(s);
            }
            for (String s : date_v.errors()) {
                v.addError(s);
            }

            // Validate time
            Validation time_v = validateTime(time, null);
            for (String s : time_v.warnings()) {
                v.addWarning(s);
            }
            for (String s : time_v.errors()) {
                v.addError(s);
            }
        } else {
            v.addError(FORMAT_MSG + FORMAT);
        }
        return v;
    }

    /**
     * Validate string as email
     * 
     * @param string
     *            //TODO comments
     * @param allowedValues
     * @return
     */
    private Validation validateEmail(String string, List<String> allowedValues) {
        Validation validation = new Validation();

        if (!emailpattern.matcher(string).matches()) {
            validation.addError(FORMAT_MSG + ". Not a valid email address");// (according
                                                                            // to
                                                                            // <a
                                                                            // href='http://www.w3.org/TR/html-markup/input.email.html'>w3.org</a>
                                                                            // as
                                                                            // of
                                                                            // 2015)");
        }

        // TODO attempt to reach the email server?

        return validation;
    }

    /**
     * Validate a string as a month
     * 
     * @param string
     *            //TODO comments
     * @param allowedValues
     * @return
     */
    private Validation validateMonth(String string, List<String> allowedValues) {
        Validation validation = new Validation();

        if (!monthpattern.matcher(string).matches()) {
            validation.addError(FORMAT_MSG + ". Month format: yyyy-mm");
        } else {
            // Regex should take care of number format
            int m_index = string.indexOf('-');
            int year = Integer.parseInt(string.substring(0, m_index));
            int month = Integer.parseInt(string.substring(m_index + 1));
            if (year <= 0)
                validation.addError(INVALID_MSG + ". Month requires a year > 0");
            if (month <= 0 || month > 12) {
                validation.addError(INVALID_MSG
                                    + ". Month must be > 0 and <= 12");
            }
        }

        return validation;
    }

    /**
     * Validate a string as a number
     * 
     * @param string
     *            //TODO comments
     * @param allowedValues
     * @return
     */
    private Validation validateNumber(String string, List<String> allowedValues) {
        Validation validation = new Validation();
        // TODO this passes some things which w3.org says shouldn't, atm I'm okay with that eg 1.e10, .1

        try {
            if (string.contains(".")) {
                Float f = Float.parseFloat(string);
            } else {
                Integer i = Integer.parseInt(string);
            }
        } catch (NumberFormatException e) {
            validation.addError(FORMAT_MSG + ". Incorrect number format");
        }

        return validation;
    }

    /**
     * Validate a string as a string. If allowedValues is null then anything is
     * allowed.
     * 
     * @param string
     *            the string to be validated
     * @param allowedValues
     *            the allowed values, set to null to allow anything
     * @return A Validation for the input.<br>
     *         A warning will be added to the validation if the string contains
     *         characters that it is recommended be escaped.<br>
     *         An error will be added to the validation if the string doesn't
     *         match an allowed value.
     */
    private Validation validateString(String string, List<String> allowedValues) {
        Validation validation = new Validation();
        // Check for html characters that should be escaped
        if (escapechars.matcher(string).find())
            validation.addWarning(ESCAPING_WARNING);

        // Check allowed values
        if (allowedValues != null && !allowedValues.contains(string)) {
            validation.addError(ALLOWED_VALUES_MSG);
        }

        return validation;
    }

    private Validation validateTel(String string, List<String> allowedValues) {
        // TODO Auto-generated method stub
        //TODO implement me
        throw new UnimplementedError();
    }

    private Validation validateURL(String string, List<String> allowedValues) {
        // TODO Auto-generated method stub
        //TODO implement me
        throw new UnimplementedError();
    }

    private Validation validateWeek(String string, List<String> allowedValues) {
        // TODO Auto-generated method stub
        //TODO implement me
        throw new UnimplementedError();
    }
}
