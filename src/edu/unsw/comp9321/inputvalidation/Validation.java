package edu.unsw.comp9321.inputvalidation;

import java.util.ArrayList;
import java.util.List;

public class Validation
{
    private final ArrayList<String> warnings = new ArrayList<String>(0);
    private final ArrayList<String> errors = new ArrayList<String>(0);
    
    public boolean addWarning(String warning){
        //TODO validate input
        return warnings.add(warning);
    }
    
    public boolean addError(String error){
        //TODO validate input
        return errors.add(error);
    }
    
    private String listToString(List<String> list, String pre, String post){
        String s = "";
        for(String l:list){
            s += pre + l + post;
        }
        return s;
    }
    
    public List<String> errors(){
        return this.errors;
    }
    
    public List<String> warnings(){
        return this.warnings;
    }
    
    public String warningsString(String pre, String post){
        return listToString(warnings, pre, post);
    }
    
    public String errorsString(String pre, String post){
        return listToString(errors, pre, post);
    }
    
    public String toString(){
        return warningsString("Warning: ", "\r\n") + errorsString("Error: ", "\r\n");
    }
    
    public boolean success(boolean warnings_as_errors){
        return this.warnings.isEmpty();
    }
    public boolean success(){
        return this.errors.isEmpty();
    }
}
