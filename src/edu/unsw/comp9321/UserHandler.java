package edu.unsw.comp9321;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import edu.unsw.comp9321.dao.GenericDAO.InsertionCheckException;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dto.UserDTO;
import edu.unsw.comp9321.inputvalidation.InputType;

public class UserHandler {
	
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	private static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for (int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	public static String hashPassword(String in, String salt) throws NoSuchAlgorithmException {
	    MessageDigest md = MessageDigest.getInstance("SHA-256");
	    md.update(salt.getBytes());
	    md.update(in.getBytes());

	    byte[] out = md.digest();
	    return bytesToHex(out);            
	}
	
	public static String generateSalt(){
		byte[] salt = new byte[64];
		Random rand = new SecureRandom();
		rand.nextBytes(salt);
		return bytesToHex(salt);
	}
	
	public static UserDTO createUser(String username, String email, String password, String passwordCheck) throws IllegalArgumentException {
		UserDTO user = new UserDTO();
		
		if(!InputType.TEXT.validate(username, null).success(true))
		    throw new IllegalArgumentException("Usernames can only contain alpha numeric characters");
		
		if (!password.equals(passwordCheck))
			throw new IllegalArgumentException("Passwords do not match");

		if (!MessageHandler.validateEmail(email))
			throw new IllegalArgumentException("Email does not exist");
		
		String salt = generateSalt();
		String pass;
		try {
			pass = hashPassword(password, salt);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException("Failed to save password");
		}
		
		user.setSalt(salt);
		user.setPassword(pass);		
		user.setUsername(username);
		user.setEmail(email);	
		return user;
	}

	public static boolean banUser(HttpServletRequest request){
		String username = request.getParameter("username");
		if (username == "admin"){
			request.setAttribute("error", "You cannot ban yourself");
			return false;
		}
		UserDAO userDAO = new UserDAO();
		UserDTO user;
		try {
			user = userDAO.findUserByUsername(username);
			if (user == null){
				request.setAttribute("error", "User: " + username + " does not exist");
				return false;
			}
			user.setBanned(true);
			userDAO.updateUser(user);
		} catch (SQLException e) {
			e.printStackTrace();
			request.setAttribute("error", "Our server is down, please try again later");
			return false;
		} catch (InsertionCheckException e) {
			e.printStackTrace();
			request.setAttribute("error", "Our server is down, please try again later");
			return false;
		} finally {
			userDAO.close();
		}
		return true;
	}
	
	public static UserDTO adminUser() {
		UserDTO admin = new UserDTO();
		admin.setConfirm(true);
		admin.setUsername("admin");
		return admin;
	}

	public static UserDTO getUser(String username) {
		UserDAO userDAO = new UserDAO();
		UserDTO user = null;
		try {
			user = userDAO.findUserByUsername(username);
		} catch (SQLException e){
			e.printStackTrace();
		}
		return user;
	}
}
