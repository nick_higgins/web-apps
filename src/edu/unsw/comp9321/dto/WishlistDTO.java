package edu.unsw.comp9321.dto;

import java.util.UUID;

public class WishlistDTO {
    private String username;
    private UUID itemID;
    
    public WishlistDTO()
    {
        username = null;
        itemID = null;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public UUID getItemID()
    {
        return itemID;
    }

    public void setItemID(UUID itemID)
    {
        this.itemID = itemID;
    }
}
