package edu.unsw.comp9321.dto;

import java.util.UUID;

public class AddressDTO 
{
    private UUID uuid;
    private String street;
    private String city;
    private String state;
    private String country;
    private int postcode;
    
    public AddressDTO()
    {
        uuid = UUID.randomUUID();
        street= null;
        city= null;
        state= null;
        country= null;
        postcode= 0;
    }

    public UUID getUuid()
    {
        return uuid;
    }

    public void setUuid(UUID uuid)
    {
        this.uuid = uuid;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public int getPostcode()
    {
        return postcode;
    }

    public void setPostcode(int postcode)
    {
        this.postcode = postcode;
    }
}
