package edu.unsw.comp9321.dto;

import java.sql.Timestamp;
import java.util.UUID;

public class ItemDTO {
    private UUID itemId;
    private String owner;
    private String title;
    private String category;
    private String picture;
    private String description;
    private UUID address;
    private float reservePrice;
    private float biddingStartPrice;
    private float biddingIncrements;
    private Currency biddingCurrency;
    private Timestamp endTime;
    private boolean isInAuction;
    private boolean isSold;
    private float currentBiddingPrice;
    private String currentBidder;
    
    public ItemDTO()
    {
        itemId = UUID.randomUUID();
        owner = null;
        title = null;
        category = null;
        picture = null;
        description = null;
        address = null;
        reservePrice = 0;
        biddingStartPrice = 0;
        biddingIncrements = 0;
        biddingCurrency = null;
        endTime = null;
        isInAuction = false;
        isSold = false;
        currentBiddingPrice = 0;
        currentBidder = null;
    }

    public UUID getItemId()
    {
        return itemId;
    }

    public void setItemId(UUID itemId)
    {
        this.itemId = itemId;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getPicture()
    {
        return picture;
    }

    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public UUID getAddress()
    {
        return address;
    }

    public void setAddress(UUID address)
    {
        this.address = address;
    }

    public float getReservePrice()
    {
        return reservePrice;
    }

    public void setReservePrice(float reservePrice)
    {
        this.reservePrice = reservePrice;
    }

    public float getBiddingStartPrice()
    {
        return biddingStartPrice;
    }

    public void setBiddingStartPrice(float biddingStartPrice)
    {
        this.biddingStartPrice = biddingStartPrice;
    }

    public float getBiddingIncrements()
    {
        return biddingIncrements;
    }

    public void setBiddingIncrements(float biddingIncrements)
    {
        this.biddingIncrements = biddingIncrements;
    }

    public Currency getBiddingCurrency()
    {
        return biddingCurrency;
    }

    public void setBiddingCurrency(Currency biddingCurrency)
    {
        this.biddingCurrency = biddingCurrency;
    }

    public Timestamp getEndTime()
    {
        return endTime;
    }

    public void setEndTime(Timestamp endTime)
    {
        this.endTime = endTime;
    }

    public boolean getIsInAuction()
    {
        return isInAuction;
    }

    public void setIsInAuction(boolean isInAuction)
    {
        this.isInAuction = isInAuction;
    }
    
    public boolean getIsSold()
    {
        return isSold;
    }

    public void setIsSold(boolean isSold)
    {
        this.isSold = isSold;
    }

    public float getCurrentBiddingPrice()
    {
        return currentBiddingPrice;
    }

    public void setCurrentBiddingPrice(float currentBiddingPrice)
    {
        this.currentBiddingPrice = currentBiddingPrice;
    }

    public String getCurrentBidder()
    {
        return currentBidder;
    }

    public void setCurrentBidder(String currentBidder)
    {
        this.currentBidder = currentBidder;
    }
}
