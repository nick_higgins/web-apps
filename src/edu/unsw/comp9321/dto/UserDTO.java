package edu.unsw.comp9321.dto;

import java.util.UUID;

public class UserDTO {
    private String username;
    private String password;
    private String salt;
    private String email;
    private String nickname;
    private String firstname;
    private String lastname;
    private int yob;
    private boolean confirm;
    private boolean banned;
	private String creditcard;
    private UUID address;
    
    public UserDTO()
    {
        username = null;
        password = null;
        salt = null;
        email = null;
        nickname = null;
        firstname = null;
        lastname = null;
        yob = 0;
        confirm = false;
        banned = false;
        creditcard = null;
        address = null;
    }
    
    
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getSalt()
    {
        return salt;
    }

    public void setSalt(String salt)
    {
        this.salt = salt;
    }


    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public int getYob()
    {
        return yob;
    }

    public void setYob(int yob)
    {
        this.yob = yob;
    }

    public boolean getConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}
    
    public boolean getBanned() {
		return banned;
	}


	public void setBanned(boolean banned) {
		this.banned = banned;
	}


	public String getCreditcard()
    {
        return creditcard;
    }

    public void setCreditcard(String creditcard)
    {
        this.creditcard = creditcard;
    }


    public UUID getAddress()
    {
        return address;
    }


    public void setAddress(UUID address)
    {
        this.address = address;
    }

}
