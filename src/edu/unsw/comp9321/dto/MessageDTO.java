package edu.unsw.comp9321.dto;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

public class MessageDTO {
    private String username;
    private Timestamp generationTime;
    private String title;
    private String message;
    private boolean isUnread;
    private UUID msgId;
    
    public MessageDTO()
    {
        username = null;
        generationTime = new Timestamp(new Date().getTime());
        title = null;
        message = null;
        isUnread = true;
        msgId = UUID.randomUUID();
    }
    
    public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public Timestamp getGenerationTime()
    {
        return generationTime;
    }

    public void setGenerationTime(Timestamp generationTime)
    {
        this.generationTime = generationTime;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public boolean isUnread()
    {
        return isUnread;
    }

    public void setUnread(boolean isUnread)
    {
        this.isUnread = isUnread;
    }

    public UUID getMsgId()
    {
        return msgId;
    }

    public void setMsgId(UUID msgId)
    {
        this.msgId = msgId;
    }

}
