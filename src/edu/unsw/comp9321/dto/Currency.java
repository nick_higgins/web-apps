package edu.unsw.comp9321.dto;

public enum Currency {
    GBP, 
    EUR, 
    USD,
    AUD;
    
    /**
     * 
     * @param s
     * @return null if s isn't a valid currency
     */
    public static Currency parse(String s){
        for(Currency c: values()){
            if(c.name().equals(s))
                return c;
        }
        return null;
    }
}
