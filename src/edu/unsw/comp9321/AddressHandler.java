/**
 *
 */
package edu.unsw.comp9321;

import java.sql.SQLException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import edu.unsw.comp9321.dao.AddressDAO;
import edu.unsw.comp9321.dto.AddressDTO;


/**
 * @author Timothy Masters
 *
 */
public class AddressHandler {
    public static AddressDTO createAddressFromRequest(HttpServletRequest request){
        AddressDTO address = new AddressDTO();
        
        if(request.getParameter("streetAddress").length() > 256)
            throw new IllegalArgumentException("Street address can be a maximum of 256 characters.");
        if(request.getParameter("city").length() > 64)
            throw new IllegalArgumentException("City can be a maximum of 64 characters.");
        if(request.getParameter("state").length() > 8)
            throw new IllegalArgumentException("State can be a maximum of 8 characters. (Please use State's initials)");
        if(request.getParameter("country").length() > 64)
            throw new IllegalArgumentException("Country can be a maximum of 64 characters.");
        
        
        if (request.getParameter("streetAddress").isEmpty())
            throw new IllegalArgumentException("Missing Street Address");
        else 
            address.setStreet(request.getParameter("streetAddress"));
        
        if (request.getParameter("city").isEmpty())
            throw new IllegalArgumentException("Missing City");
        else 
            address.setCity(request.getParameter("city"));
        
        if (request.getParameter("state").isEmpty())
            throw new IllegalArgumentException("Missing State");
        else 
            address.setState(request.getParameter("state"));
        
        if (request.getParameter("country").isEmpty())
            throw new IllegalArgumentException("Missing Country");
        else 
            address.setCountry(request.getParameter("country"));
        
        try {
            address.setPostcode(Integer.parseInt(request.getParameter("postalCode")));
        } catch (NumberFormatException e){
            throw new IllegalArgumentException("Invalid Postal Code");          
        }
        
        return address;
    }

	public static AddressDTO getAddress(UUID address) {
		AddressDAO addrDAO = new AddressDAO();
		AddressDTO addr = null;
		try {
			addr = addrDAO.getAddress(address);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			addrDAO.close();
		}
		return addr;
	}
}
