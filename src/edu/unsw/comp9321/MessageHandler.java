package edu.unsw.comp9321;

import java.sql.SQLException;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import edu.unsw.comp9321.dao.GenericDAO.InsertionCheckException;
import edu.unsw.comp9321.dao.MessageDAO;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dto.MessageDTO;
import edu.unsw.comp9321.dto.UserDTO;

public class MessageHandler {

	private final static String ourEmail = "auction.site9321@gmail.com";
	private final static String ourPass = "tomato47";
	
	public static boolean validateEmail(String addr){
		try {
			InternetAddress inetAddr = new InternetAddress(addr);
			inetAddr.validate();
			return true;
		} catch (AddressException e){
			e.printStackTrace();
		}
		return false;
	}
	
	public static int countUnread(String username){
		MessageDAO msgDAO = new MessageDAO();
		try {
		    int count = msgDAO.countUnread(username);
		    msgDAO.close();
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		msgDAO.close();
		return 0;
	}
	
	public static boolean sendMessage(MessageDTO messageDTO){
		UserDAO userDAO = new UserDAO();
		UserDTO user;
		try {
			user = userDAO.findUserByUsername(messageDTO.getUsername());
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
		    userDAO.close();
		}
		if (!validateEmail(user.getEmail()))
			return false;
		
      Properties props = System.getProperties();
  	  props.put("mail.smtp.auth", "true");
	  props.put("mail.smtp.starttls.enable", "true");
	  props.put("mail.smtp.host", "smtp.gmail.com");
	  props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(ourEmail, ourPass);
			}
		  });
		
      try{
         MimeMessage message = new MimeMessage(session);
         message.setFrom(new InternetAddress(ourEmail));
         message.addRecipient(Message.RecipientType.TO,
	                                  new InternetAddress(user.getEmail()));

         message.setSubject("Update from Auction Site");
         message.setText(messageDTO.getMessage());

         Transport.send(message);
      }catch (MessagingException e) {
         e.printStackTrace();
         return false;
      }	
      return true;
	}
	
	
	public static boolean sendConfirmation(String username, String confLink){	
		MessageDTO message = new MessageDTO();
		message.setUsername(username);

		String body = "Welcome " + username + " to the auction site\n"
				+ "You have created an account on our site and now need to confirm this is you!\n"
				+ "To do so please click the link below:\n"
				+ "\n"
				+ confLink + "\n"
				+ "\n";
		message.setMessage(body);
		message.setTitle("Confirmation");
		return sendMessage(message);
	}

	public static MessageDTO checkUnread(String username) {
		MessageDAO msgDAO = new MessageDAO();
		MessageDTO msg = null;
		try {
			msg = msgDAO.findUnreadByUsername(username);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		msgDAO.close();
		return msg;
	}
	
	/**
	 * 
	 * @param username
	 * @param title
	 * @param message
	 * @param waitForEmail
	 * @return
	 */
	public static boolean createMessage(String username, String title, String message, boolean waitForEmail) {
		MessageDTO msg = new MessageDTO();
		msg.setTitle(title);
		msg.setUsername(username);
		msg.setMessage(message);
		
		MessageDAO msgDAO = new MessageDAO();
		try {
			msgDAO.addMessage(msg);
		} catch (InsertionCheckException e) {
			e.printStackTrace();
			msgDAO.close();
			return false;
		} catch (SQLException e) {			
			e.printStackTrace();
			msgDAO.close();
			return false;
		}
		msgDAO.close();
		if(waitForEmail)
		    return sendMessage(msg);
		else{
		    MessageSenderThread msgSender = new MessageSenderThread(msg);
		    msgSender.start();
		    return true;
		}
	}
	
	public static String getBaseURL(HttpServletRequest request) {
		String scheme = request.getScheme();
        String serverName = request.getServerName();
        int serverPort = request.getServerPort();
        String uri = (String) request.getContextPath();
        return scheme + "://" + serverName + ":" + serverPort + uri;
	}

	public static void removeMessage(String username, String itemId) throws IllegalArgumentException, SQLException {
		MessageDAO msgDAO = new MessageDAO();
		MessageDTO msg = msgDAO.findMessageById(UUID.fromString(itemId));
		if (msg == null){
			msgDAO.close();
			throw new IllegalArgumentException("Message does not exist");
		}
		
		if (msg.getUsername().equals(username)){
			msgDAO.deleteMessageById(UUID.fromString(itemId));
		} else {
			msgDAO.close();
			throw new IllegalArgumentException("Message does not belong to you");
		}
		msgDAO.close();
	}
}
