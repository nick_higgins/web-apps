package edu.unsw.comp9321;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import edu.unsw.comp9321.dao.AddressDAO;
import edu.unsw.comp9321.dao.ItemDAO;
import edu.unsw.comp9321.dao.MessageDAO;
import edu.unsw.comp9321.dao.PostgresConnFactory;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dao.WishlistDAO;
import edu.unsw.comp9321.dto.AddressDTO;
import edu.unsw.comp9321.dto.ItemDTO;
import edu.unsw.comp9321.dto.MessageDTO;
// import edu.unsw.comp9321.dao.impl.UserDAOImpl;
import edu.unsw.comp9321.dto.UserDTO;

/**
 * Servlet implementation class ControlServlet
 */
@MultipartConfig
public class ControlServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static ScheduledExecutorService scheduler;
    Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlServlet() {
        super();
    }

    protected String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                s = s.substring(s.indexOf("=") + 2);
                return s.substring(s.lastIndexOf(File.separator)+1, s.length() - 1);
            }
        }
        return "";
    }

    protected String uploadPicture(HttpServletRequest request) {
        Part part = null;
        try {
            part = request.getPart("uploadPicture");
        } catch (IllegalStateException e) {
            logger.severe(e.getMessage());
            return "";
        } catch (IOException e) {
            logger.severe(e.getMessage());
            return "";
        } catch (ServletException e) {
            logger.severe(e.getMessage());
            return "";
        }

        if (part == null) {
            try {
                for (Part p : request.getParts()) {
                    logger.info("Part name: " + p.getName());
                }
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ServletException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return "";
        }
        String fileName = "";
        fileName = extractFileName(part);
        logger.info("File Name: " + fileName);

        String savePath = request.getServletContext().getRealPath("")
                          + File.separator
                          + "img";

        File fileSaveDir = new File(savePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }

        try {
            part.write(savePath + File.separator + fileName);
        } catch (IOException e) {
            logger.severe(e.getMessage());
            return "";
        }
        return "img" + File.separator + fileName;
    }

    /**
     * @see Servlet#init(ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {
        logger.info("Performing server initialization actions");
        try {
            ServletContext application = config.getServletContext();
        } catch (Exception err) {
            logger.severe("Error initializing connection factory: "
                          + err.getMessage());
        }
        
        scheduler = Executors.newSingleThreadScheduledExecutor();
        Runnable auctionChecker = new AuctionEndChecker();
        scheduler.scheduleAtFixedRate(auctionChecker, 5, 30, TimeUnit.SECONDS);
        
        logger.info("Server initialization complete");
    }

    /**
     * @see Servlet#destroy()
     */
    public void destroy() {
        scheduler.shutdown();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
                                                      IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                       IOException {
        request.setAttribute("redirect", false);
        
        String action = request.getParameter("action");
        String nextPage = "index.jsp";
        logger.info("Action: " + action);
        HttpSession session = request.getSession();
        request.removeAttribute("error");
        if (action == null) {
        	addItems(request);
        } else if (action.equals("destroy")){
        	session.invalidate();
            request.setAttribute("redirect", true);        	
        } else if (action.equals("signmeup")) {
            nextPage = "signup.jsp";
            request.setAttribute("redirect", true);
        } else if (action.equals("login")) {
            if (request.getParameter("username") != null){
                try {
                    nextPage = doLogin(request);
                    addItems(request);
                } catch (IllegalArgumentException e) {
                    request.setAttribute("error", e.getMessage());
                    nextPage = "login.jsp";
                }
            } else {
                nextPage = "login.jsp";
                request.setAttribute("redirect", true);
            }
        } else if (action.equals("admin")) {
        	if (request.getParameter("password").equals("tomato")){
        		//Super secret tomato password
        		session.setAttribute("admin", true);
        		session.setAttribute("username", "admin");
        		nextPage = "index.jsp";
        		addItems(request);
        	} else {
        		request.setAttribute("error", "You do not have admin access");
        		nextPage = "admin.jsp";
                request.setAttribute("redirect", true);        		
        	}
        } else if (action.equals("signup")) {
        	try {
        		nextPage = doSignUp(request);
        	} catch (IllegalArgumentException e){
        		request.setAttribute("error", e.getMessage());
        		nextPage = "signup.jsp";        		
        	}
        } else if (action.equals("confirmUser")){
        	UserDAO userDAO = new UserDAO();
        	try {
				UserDTO user = userDAO.findUserByUsername(request.getParameter("username"));
				user.setConfirm(true);
				userDAO.updateUser(user);				
        	} catch (SQLException e) {
				request.setAttribute("error", "Our server is down... please come back later");				
				e.printStackTrace();
			} finally {
			    userDAO.close();
			}
        	nextPage = "home.jsp";
        } else if (action.equals("view")) {
        	ItemDAO itemDAO = new ItemDAO();
        	AddressDAO addrDAO = new AddressDAO();
        	try {
        		ItemDTO item = itemDAO.findItemById(UUID.fromString(request.getParameter("itemId")));
        		AddressDTO addr = addrDAO.getAddress(item.getAddress());
        		logger.info("Viewing item: " + item.getItemId());
        		request.setAttribute("item", item);
        		request.setAttribute("addr", addr);
        		AuctionItemHandler.handleViewItemFromRequest(request, session);
        		nextPage = "view.jsp";
			} catch (SQLException e) {
				request.setAttribute("error", "Our server is down... please come back later");
				e.printStackTrace();
			} finally {
			    itemDAO.close();
			    addrDAO.close();
			}
        } else if (action.equals("search")) {
        	ItemDAO itemDAO = new ItemDAO();
        	try {
        		request.setAttribute("items", makeViewable(itemDAO.searchItems((String) request.getParameter("search"))));
        	} catch (SQLException e) {
				request.setAttribute("error", "Our server is down... please come back later");
				e.printStackTrace();
        	}finally {
                itemDAO.close();
            }
        	nextPage = "search.jsp";
        } else if (session.getAttribute("username") == null) {
            nextPage = "login.jsp";
            request.setAttribute("redirect", true);
        } else {
        	//Requires user be logged in access
        	UserDAO userDAO = new UserDAO();
        	UserDTO user = null;
			try {
				user = userDAO.findUserByUsername((String) session.getAttribute("username"));
			} catch (SQLException e2) {
				e2.printStackTrace();
			} finally {
			    userDAO.close();			    
			}
        	if (session.getAttribute("admin") != null && session.getAttribute("admin").equals(true)){
        		user = UserHandler.adminUser();
        	}
        	if (user == null) {
        		request.setAttribute("error", "User " + session.getAttribute("username") + " is not valid");
        		nextPage = "index.jsp";
        		addItems(request);
        	} else if (action.equals("logout")) {
	            session.removeAttribute("username");
	            session.removeAttribute("admin");
	            nextPage = "login.jsp";
	            request.setAttribute("redirect", true);
        	} else if (!user.getConfirm()) {
        		request.setAttribute("error", "User " + session.getAttribute("username") + " is not valid, please check your email");
        		nextPage = "index.jsp";
        		addItems(request);
        	} else if (action.equals("updateUser")) {
	            nextPage = "UpdateUser.jsp";
	            AddressDAO addrDAO = new AddressDAO();
				try {
		            AddressDTO addr = addrDAO.getAddress(user.getAddress());
		            request.setAttribute("user", user);
		            request.setAttribute("addr", addr);
		            logger.info("User: " + user.getEmail());
				} catch (SQLException e) {
					request.setAttribute("error",
	                        "Our server is down... please come back later");
					logger.severe(e.getMessage());
					nextPage = "home.jsp";
				} finally {
				    addrDAO.close();
				}
	        } else if (action.equals("submitUser")) {
	            nextPage = "UpdateUser.jsp";
	            try {
	            	doUpdateUser(request);
	            	ItemDAO itemDAO = new ItemDAO();
	            	try {
	    				request.setAttribute("items", makeViewable(itemDAO.findItemsByUser((String) session.getAttribute("username"))));
	    			} catch (SQLException e) {
	    				request.setAttribute("error", "Our server is down... please come back later");
	    				e.printStackTrace();
	    			} finally {
	    			    itemDAO.close();
	    			}
	            	nextPage = "home.jsp";
	            } catch (IllegalArgumentException e){
	                AddressDAO addrDAO = new AddressDAO();
	    			try {
	    	            AddressDTO addr = addrDAO.getAddress(user.getAddress());
	    	            request.setAttribute("user", user);
	    	            request.setAttribute("addr", addr);
	    			} catch (SQLException e1){
	    				request.setAttribute("error", "Our server is down... please come back later");
	    				logger.severe(e1.getMessage());
	    			} finally {
	    			    addrDAO.close();
	    			}
	    			request.setAttribute("error", e.getMessage());
	            }
	        } else if (action.equals("add")) {
	            nextPage = "add.jsp";
	            request.setAttribute("redirect", true);
	        } else if (action.equals("submit")) {
	            nextPage = doAddItem(request);
	            //addItems(request);
	        } else if (action.equals("wishlist")) {
	        	try {
	            	request.setAttribute("items", makeViewable(WishlistHandler.getWishlistByUser((String) session.getAttribute("username"))));        		
	        	} catch (SQLException e) {
					request.setAttribute("error", "Our server is down... please come back later");
					e.printStackTrace();        		
	        	}
	        	nextPage = "wishlist.jsp";
	        } else if (action.equals("addWishlist")) {
	        	try {
	        		WishlistHandler.addItem((String)session.getAttribute("username"),
	        			UUID.fromString(request.getParameter("itemId")));
	        	} catch (IllegalArgumentException e){
	        		request.setAttribute("error", e.getMessage());
	        	}
	        	logger.info("Item added to wishlist");
	        	try {
        	    
	            	request.setAttribute("items", makeViewable(WishlistHandler.getWishlistByUser((String) session.getAttribute("username"))));        		
	        	} catch (SQLException e) {
					request.setAttribute("error", "Our server is down... please come back later");
					e.printStackTrace();        		
	        	}        	
	        	nextPage = "wishlist.jsp";
	        } else if (action.equals("removeWishlist")) {
	        	WishlistHandler.removeItem((String)session.getAttribute("username"),
	        			UUID.fromString(request.getParameter("itemId")));
	        	nextPage = "wishlist.jsp";
	        	
	        userDAO.close();
        } else if (action.equals("auction_bid")){
            String previousBidder = AuctionItemHandler.HandleBid(request, session);
            if (previousBidder != null) {
                //send email and generate message for current bidder and previous bidder.
                
            }
            AuctionItemHandler.handleViewItemFromRequest(request, session);
            nextPage = "view.jsp";
        } else if (action.equals("auction_start")){
            if (AuctionItemHandler.StartAuction(request, session)){
              //send email and generate message for owner.
            }
            AuctionItemHandler.handleViewItemFromRequest(request, session);
            nextPage = "view.jsp";
        } else if (action.equals("haltAuction")){
        	if (session.getAttribute("admin") != null && session.getAttribute("admin").equals(true)){
        		AuctionItemHandler.haltAuction(request);
        	} else {
        		request.setAttribute("error", "You are not admin, you cannot do this");
        	}
        	nextPage = "view.jsp";
        } else if (action.equals("banUser")){
        	if (session.getAttribute("admin") != null && session.getAttribute("admin").equals(true)){
            	UserHandler.banUser(request);
        	} else {
        		request.setAttribute("error", "You are not admin, you cannot do this");        		
        	}
        } else if (action.equals("home")){
	        	ItemDAO itemDAO = new ItemDAO();
	        	try {
					request.setAttribute("items", makeViewable(itemDAO.findItemsByUser((String) session.getAttribute("username"))));
				} catch (SQLException e) {
					request.setAttribute("error", "Our server is down... please come back later");
					e.printStackTrace();
				} finally {
				    itemDAO.close();
				}
	        	nextPage = "home.jsp";
	        } else if (action.equals("messages")){
	        	MessageDAO msgDAO = new MessageDAO();
	        	try {
					request.setAttribute("messages", msgDAO.findAllByUsername((String)session.getAttribute("username")));
		        	nextPage = "messages.jsp";
	        	} catch (SQLException e) {
					request.setAttribute("error", "Our server is down... please come back later");
					e.printStackTrace();
				} finally {
				    msgDAO.close();
				}
	        	nextPage = "messages.jsp";
	        } else if (action.equals("viewMessage")){
	        	MessageDAO msgDAO = new MessageDAO();
	        	try {
					MessageDTO msg = msgDAO.findMessageById(UUID.fromString(request.getParameter("msgId")));
					if (msg != null){
						msg.setUnread(false);
						msgDAO.updateMessage(msg);
						request.setAttribute("msg", msg);
					} else {
						request.setAttribute("error", "Message could not be found");
					}
				} catch (SQLException e) {
					request.setAttribute("error", "Our server is down... please come back later");
					e.printStackTrace();
				} finally {
				    msgDAO.close();
				}
	        	nextPage = "viewMessage.jsp";
	        } else if (action.equals("checkUnread")){
	        	String username = request.getParameter("username");
	        	if (username != null && userDAO.exists(username)){
	        		MessageDAO msgDAO = new MessageDAO();
	        		MessageDTO msg = MessageHandler.checkUnread(username);
	        		if (msg != null){
	        			response.getWriter().print(msg.getMessage());
	        			msg.setUnread(false);
	        			msgDAO.updateMessage(msg);
	        		} else {
	        			response.sendError(404);
	        		}
	        	} else {
        			response.sendError(404);	          			
	        	}
	        	return;
	        } else {
	        	addItems(request);
	        }
        }
        
        //Housekeeping stuff
        if (session.getAttribute("username") != null) {
        	session.setAttribute("unreadMessages", MessageHandler.countUnread((String) session.getAttribute("username")));
        } else {
        	session.setAttribute("unreadMessages", 0);
        }
        
        /*
         * Go to whatever has been selected as the next page.
         */
        boolean redirect = (request.getAttribute("redirect") != null) ? (Boolean)request.getAttribute("redirect") : false;
        if(redirect){
            logger.info("Redirecting");
            response.sendRedirect(nextPage);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("/" + nextPage);
            rd.forward(request, response);
        }
    }

    private void addItems(HttpServletRequest request) {
        ItemDAO itemDAO = new ItemDAO();
        try {
            request.setAttribute("items", makeViewable(itemDAO.findSome(12)));
        } catch (SQLException e) {
            request.setAttribute("error",
                                 "Our server is down... please come back later");
            e.printStackTrace();
        }finally {
            itemDAO.close();
        }
    }
    
    private ArrayList<ArrayList<ItemDTO>> makeViewable(ArrayList<ItemDTO> items){
    	logger.info("Making viewable");
    	ArrayList<ArrayList<ItemDTO>> viewable = new ArrayList<ArrayList<ItemDTO>>();
    	int count = 0;
    	int rows = 0;
    	for (ItemDTO item : items){
    		if (count == 0)
    			viewable.add(new ArrayList<ItemDTO>());
    		viewable.get(rows).add(item);
    		count++;
    		if (count == 3){
    			count = 0;
    			rows++;
    		}
    	}
    	return viewable;
    }
    
    /**
     * @param request
     * @return
     */
    private String doAddItem(HttpServletRequest request) {
        String pic = uploadPicture(request);

        Connection conn = PostgresConnFactory.getConnection();
        try {
            ItemDTO item = AuctionItemHandler.createItemFromRequest(request,
                                                                    pic);
            
            AddressDTO address = AddressHandler.createAddressFromRequest(request);
            
            
            AddressDAO addrdao = new AddressDAO();
            if(addrdao.insertAddress(address)){
                item.setAddress(address.getUuid());
                
                ItemDAO itemdao = new ItemDAO();
                if(itemdao.AddItem(item)){
                    request.setAttribute("redirect", true);
                    request.setAttribute("success", "Thank you for adding an item");
                    addrdao.close();
                    itemdao.close();
                    return "control?action=view&itemId=" + item.getItemId();
                } else {
                    //TODO some error?
                    addrdao.close();
                    itemdao.close();
                    logger.info("Failed to add item to db");
                    request.setAttribute("error", "There was a problem adding the item to the database");
                }
            } else {
                //TODO some error?
                addrdao.close();
                logger.info("Failed to add item: Failed to add address");
                request.setAttribute("error", "There was a problem adding the address to the database");
            }
        } catch (IllegalArgumentException e) {
            //TODO add error or something;
            logger.info("Failed to add item: " + e.getMessage());
            request.setAttribute("error", "Failed to add item: " + e.getMessage());
        }
        
        try {
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "add.jsp";
    }

    private String doUpdateUser(HttpServletRequest request) throws IllegalArgumentException {
    	String nextPage = "home.jsp";
    	UserHandler.updateUser(request);
    	return nextPage;
    }
    
    /**
     * @param request
     * @return
     */
    private String doLogin(HttpServletRequest request) throws IllegalArgumentException {
        String next_page = "login.jsp";

        // TODO sanitize/validate input
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Connection conn = PostgresConnFactory.getConnection();
        UserDAO dao = new UserDAO();

        try {
            UserDTO user = dao.findUserByUsername(username);
            dao.close();
            if (user != null) {
                //Check passwords
                String salt = user.getSalt();
                String hash = UserDAO.hashPassword(password, salt);

                if (hash.equals(user.getPassword())) {
                	if (user.getConfirm()){
                    	HttpSession session = request.getSession();
                        session.setAttribute("username", user.getUsername());
                	} else {
                		request.setAttribute("error", "User " + username + " is not confirmed yet, check your email");                		
                	}
                    next_page = "index.jsp"; //TODO send them back where they came from?                	
                } else {
                    logger.info("Couldn't sign user in, incorrect password: "
                                + username
                                + ", "
                                + password);
                    throw new IllegalArgumentException("The username or password is incorrect");
                }
            } else {
                logger.info("Couldn't sign user in, user not found:" + username);
                throw new IllegalArgumentException("The username or password is incorrect");
            }
        } catch (SQLException e) {
            dao.close();
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new IllegalArgumentException("SQL Exception: "
                                               + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            dao.close();
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new IllegalArgumentException("Hash Exception: "
                                               + e.getMessage());
        }

        //TODO email confirmation stuff

        return next_page;
    }

    /**
     * @param request
     * @return
     */
    private String doSignUp(HttpServletRequest request) throws IllegalArgumentException {
        //TODO sanitize/validate input

        UserDTO user = null;
        String next_page = "signup.jsp";

        try {
            user = UserHandler.createUser(request.getParameter("username"),
                                          request.getParameter("email"),
                                          request.getParameter("password"),
                                          request.getParameter("passwordCheck"));
        } catch (IllegalArgumentException e) {
            logger.info("Failed user creation: " + e.getMessage());
            throw e;
        }

        UserDAO dao = new UserDAO();
        if (user != null && dao.addUser(user)) {
            dao.close();
            //System.err.println("Signed up " + request.getParameter("username"));
            HttpSession session = request.getSession();
            session.setAttribute("username", user.getUsername());

            //TODO User needs a confirmed field and message needs a title field
            String scheme = request.getScheme();             
            String serverName = request.getServerName(); 
            int serverPort = request.getServerPort();    
            String uri = (String) request.getContextPath() + "/control";
            MessageHandler.sendConfirmation(user.getUsername(), scheme + "://" +serverName + ":" + serverPort + uri + "?action=confirmUser&user="+user.getUsername());
            request.setAttribute("redirect", true);

            next_page = "index.jsp"; //TODO send to profile page to finish filling out details
        } else if (user != null) {
            dao.close();
            logger.info("User signup failed:" + user);
            throw new IllegalArgumentException("User signup failed");
        }
        
        return next_page;
    }

}
