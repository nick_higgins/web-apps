package edu.unsw.comp9321.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.UUID;

import edu.unsw.comp9321.dto.ItemDTO;
import edu.unsw.comp9321.dto.Currency;
import edu.unsw.comp9321.inputvalidation.InputType;

public class ItemDAO extends GenericDAO {
    public class ItemNotFoundException extends Exception {
        private static final long serialVersionUID = 1L;

        public ItemNotFoundException(String message) {
            super(message);
        }
    }

    public ItemDAO() {
        super();
    }

    private ItemDTO fromResult(ResultSet result) throws SQLException {
        ItemDTO item = new ItemDTO();
        item.setItemId((UUID) result.getObject("itemId"));
        if (result.getString("owner") != null)
            item.setOwner(InputType.removeSlashes(result.getString("owner")));
        if (result.getString("title") != null)
            item.setTitle(InputType.removeSlashes(result.getString("title")));
        if (result.getString("category") != null)
            item.setCategory(InputType.removeSlashes(result.getString("category")));
        if (result.getString("picture") != null)
            item.setPicture(InputType.removeSlashes(result.getString("picture")));
        if (result.getString("description") != null)
            item.setDescription(InputType.removeSlashes(result.getString("description")));
        item.setAddress((UUID) result.getObject("address"));
        item.setReservePrice(result.getFloat("reservePrice"));
        item.setBiddingStartPrice(result.getFloat("biddingStartPrice"));
        item.setBiddingIncrements(result.getFloat("biddingIncrements"));
        item.setBiddingCurrency(result.getString("biddingCurrency") != null ? Currency.valueOf(result.getString("biddingCurrency")) : null);
        item.setEndTime(result.getTimestamp("endTime"));
        item.setIsInAuction(result.getBoolean("isInAuction"));
        item.setIsSold(result.getBoolean("isSold"));
        item.setCurrentBiddingPrice(result.getFloat("currentBiddingPrice"));
        item.setCurrentBidder(result.getString("currentBidder"));
        return item;
    }

    public ArrayList<ItemDTO> findAll() throws SQLException {
        ArrayList<ItemDTO> itemList = new ArrayList<ItemDTO>();
        Statement query = getStatement();
        if (query == null)
            return null;
        ResultSet result = query.executeQuery("SELECT * FROM items ORDER BY itemId ASC");
        while (result.next())
            itemList.add(fromResult(result));

        return itemList;
    }

    public ArrayList<ItemDTO> findSome(int number) throws SQLException {
        ArrayList<ItemDTO> itemList = new ArrayList<ItemDTO>();
        Statement query = getStatement();
        if (query == null)
            return null;
        ResultSet result = query.executeQuery("SELECT * FROM items ORDER BY RANDOM() LIMIT "
                                              + String.valueOf(number));
        while (result.next())
            itemList.add(fromResult(result));
        return itemList;
    }

    public ItemDTO findItemById(UUID itemId) throws SQLException,
                                            ItemNotFoundException {
        PreparedStatement query = getPreparedStatement("SELECT * FROM items WHERE itemId=?");

        query.setObject(1, itemId);

        ResultSet result = query.executeQuery();
        
        if (!result.next()) {
            logger.warning("No record for the given UUID is found:" + itemId);
            throw new ItemNotFoundException("That item was not found");
        }

        ItemDTO item = fromResult(result);
        
        if (result.next())
            logger.warning("More than one record retrieved for given UUID, returning first record.");

        return item;
    }

    public ArrayList<ItemDTO> searchItems(String s) throws SQLException {
        ArrayList<ItemDTO> itemList = new ArrayList<ItemDTO>();
        Statement query = getStatement();
        String[] strings = s.split(" ");
        String qString = "(";
        for (String q : strings) {
            qString += q.toLowerCase();
            if (strings[strings.length - 1].compareTo(q) != 0) {
                qString += "|";
            }
        }
        qString += ")";
        if (query == null)
            return null;
        ResultSet result = query.executeQuery(""
                                              + "SELECT * FROM items WHERE lower(title) SIMILAR TO '%"
                                              + qString
                                              + "%' "
                                              + "OR lower(category) SIMILAR TO '%"
                                              + qString
                                              + "%' "
                                              + "OR lower(description) SIMILAR TO '%"
                                              + qString
                                              + "%'");
        while (result.next())
            itemList.add(fromResult(result));
        logger.info("Returned " + itemList.size() + " items");
        return itemList;
    }

    public ArrayList<ItemDTO> findItemsByUser(String userId) throws SQLException {
        ArrayList<ItemDTO> items = new ArrayList<ItemDTO>();
        PreparedStatement query = getPreparedStatement("SELECT * FROM items WHERE owner=?");
        if (query == null)
            return null;
        query.setObject(1, userId);
        ResultSet result = query.executeQuery();
        while (result.next())
            items.add(fromResult(result));

        return items;
    }

    public void UpdateItem(ItemDTO item) throws SQLException {
        PreparedStatement statement = getPreparedStatement("UPDATE items "
                                                           + "SET "
                                                           + "owner=?, "
                                                           + "title=?, "
                                                           + "category=?,"
                                                           + "picture=?,"
                                                           + "description=?,"
                                                           + "address=?,"
                                                           + "reserveprice=?,"
                                                           + "biddingstartprice=?,"
                                                           + "biddingincrements=?,"
                                                           + "biddingcurrency=cast(? as Currency),"
                                                           + "endtime=?,"
                                                           + "isinauction=?,"
                                                           + "issold=?,"
                                                           + "currentbiddingprice=?,"
                                                           + "currentbidder=? "
                                                           + "WHERE itemid=?");

        int index = 0;
        statement.setString(++index, item.getOwner());
        statement.setString(++index, InputType.addSlashes(item.getTitle()));
        statement.setString(++index, InputType.addSlashes(item.getCategory()));
        statement.setString(++index, InputType.addSlashes(item.getPicture()));
        statement.setString(++index,
                            InputType.addSlashes(item.getDescription()));
        statement.setObject(++index, item.getAddress());
        statement.setFloat(++index, item.getReservePrice());
        statement.setFloat(++index, item.getBiddingStartPrice());
        statement.setFloat(++index, item.getBiddingIncrements());
        if (item.getBiddingCurrency() == null)
            statement.setNull(++index, Types.OTHER);
        else
            statement.setObject(++index, item.getBiddingCurrency().toString());
        statement.setObject(++index, item.getEndTime());
        statement.setBoolean(++index, item.getIsInAuction());
        statement.setBoolean(++index, item.getIsSold());
        statement.setFloat(++index, item.getCurrentBiddingPrice());
        statement.setString(++index, item.getCurrentBidder());
        statement.setObject(++index, item.getItemId());

        statement.executeUpdate();
    }

    public void AddItem(ItemDTO item) throws SQLException {
        PreparedStatement statement = getPreparedStatement("INSERT INTO items ("
                                                           + "itemid, "
                                                           + "owner, "
                                                           + "title, "
                                                           + "category,"
                                                           + "picture,"
                                                           + "description,"
                                                           + "address,"
                                                           + "reserveprice,"
                                                           + "biddingstartprice,"
                                                           + "biddingincrements,"
                                                           + "biddingcurrency,"
                                                           + "endtime,"
                                                           + "isinauction,"
                                                           + "issold,"
                                                           + "currentbiddingprice,"
                                                           + "currentbidder)"
                                                           + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, cast(? as Currency), ?, ?, ?, ?, ?)");

        int index = 0;
        statement.setObject(++index, item.getItemId());
        statement.setString(++index, item.getOwner());
        statement.setString(++index, InputType.addSlashes(item.getTitle()));
        statement.setString(++index, InputType.addSlashes(item.getCategory()));
        statement.setString(++index, InputType.addSlashes(item.getPicture()));
        statement.setString(++index,
                            InputType.addSlashes(item.getDescription()));
        statement.setObject(++index, item.getAddress());
        statement.setFloat(++index, item.getReservePrice());
        statement.setFloat(++index, item.getBiddingStartPrice());
        statement.setFloat(++index, item.getBiddingIncrements());
        if (item.getBiddingCurrency() == null)
            statement.setNull(++index, Types.OTHER);
        else
            statement.setObject(++index, item.getBiddingCurrency().toString());
        statement.setObject(++index, item.getEndTime());
        statement.setBoolean(++index, item.getIsInAuction());
        statement.setBoolean(++index, item.getIsSold());
        statement.setFloat(++index, item.getCurrentBiddingPrice());
        statement.setString(++index, item.getCurrentBidder());

        statement.executeUpdate();
    }

    public boolean exists(UUID id) {
        try {
            return findItemById(id) != null;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ItemNotFoundException e) {
        }
        return false;
    }

	public void deleteItemById(UUID itemId) throws SQLException {
        PreparedStatement query = getPreparedStatement("DELETE FROM items WHERE itemId=?");
        query.setObject(1, itemId);
        query.execute();
	}    
}
