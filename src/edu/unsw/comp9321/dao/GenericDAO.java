package edu.unsw.comp9321.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public abstract class GenericDAO {
    public class InsertionCheckException extends Exception {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        public InsertionCheckException(String message) {
            super(message);
        }
        
        public InsertionCheckException(){
            super();
        }
    }

    private Connection dbConn;
    protected Logger   logger;

    protected GenericDAO() {
        this.dbConn = PostgresConnFactory.getConnection();
        this.logger = Logger.getLogger(this.getClass().getName());
    }

    /**
     * @see Connection#createStatement
     * @return
     * @throws SQLException
     */
    protected Statement getStatement() throws SQLException {
        return this.dbConn.createStatement();
    }

    protected PreparedStatement getPreparedStatement(String insertion) throws SQLException {
        return this.dbConn.prepareStatement(insertion);
    }

    public final void close() {
        try {
            this.dbConn.close();
            PostgresConnFactory.closedConnection();
        } catch (SQLException e) {
            logger.info("dbConn already closed");
            e.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        boolean alreadyClosed = false;
        try {
            this.dbConn.close();
        } catch (SQLException e) {
            alreadyClosed = true;
        }

        if (!alreadyClosed) {
//            this.logger.severe("DAO had open SQL connection on garbage collection:" + this.getClass().getName());
        }
    }
}
