package edu.unsw.comp9321.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.naming.*;
import javax.sql.DataSource;

public class PostgresConnFactory extends AbstractJndiLocator 
{
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private long t = System.currentTimeMillis();
    private static int open_connections = 0;
    //Not sure I like this being public but it might be useful for testing
    public static final PostgresConnFactory THE_FACTORY = new PostgresConnFactory();

    //Should remain private
    private DataSource ds;
    
    //Should remain private, we should only ever need the one instance of PostgresConnFactory
    private PostgresConnFactory()
    {
        super();
        setDataSource();
    }
    
    //Should remain private
    private Connection _getConnection()
    {

        open_connections++;
        if(System.currentTimeMillis() > (t + 10000)){
            //logger.warning(open_connections + " open SQL connections");
            t = System.currentTimeMillis();
        }
        try 
        {
            return ds.getConnection();
        } 
        catch (SQLException err) 
        {
            open_connections--;
            throw new Error("Unable to create connection: " + err.getMessage(), err);
        }
    }
    
    public static void closedConnection(){
        open_connections --;
    }
    
    public static int countOpenConnection(){
        return open_connections;
    }
    //Should remain private
    private void setDataSource()
    {
        setDataSource("jdbc/Postgresql");
    }
    
    //Should remain private
    private void setDataSource(String contextString ) 
    {
        try 
        {
            ds = (DataSource) lookup(contextString);
        } 
        catch (NamingException e) 
        {
            throw new Error("Unable to locate data source; " + e.getMessage(), e);
        }
    }
    
    /**
     * Get a connection
     * @return
     */
    public static Connection getConnection(){
        return THE_FACTORY._getConnection();
    }
}
