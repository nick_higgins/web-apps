package edu.unsw.comp9321.dao;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import edu.unsw.comp9321.dto.UserDTO;
import edu.unsw.comp9321.inputvalidation.InputType;

public class UserDAO extends GenericDAO {

    public UserDAO() {
        super();
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String hashPassword(String in, String salt) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(salt.getBytes());
        md.update(in.getBytes());

        byte[] out = md.digest();
        return bytesToHex(out);
    }

    public static String generateSalt() {
        byte[] salt = new byte[64];
        Random rand = new SecureRandom();
        rand.nextBytes(salt);
        return bytesToHex(salt);
    }

    public UserDTO fromResult(ResultSet result) throws SQLException {
        UserDTO user = new UserDTO();
        user.setUsername(result.getString("username"));
        user.setPassword(result.getString("password"));
        user.setSalt(result.getString("salt"));
        if (result.getString("email") != null)
            user.setEmail(InputType.removeSlashes(result.getString("email")));
        if (result.getString("nickname") != null)
            user.setNickname(InputType.removeSlashes(result.getString("nickname")));
        if (result.getString("firstname") != null)
            user.setFirstname(InputType.removeSlashes(result.getString("firstname")));
        if (result.getString("lastname") != null)
            user.setLastname(InputType.removeSlashes(result.getString("lastname")));
        user.setYob(result.getInt("yob"));
        user.setConfirm(result.getBoolean("confirm"));
        user.setBanned(result.getBoolean("banned"));
        if (result.getString("creditcard") != null)
            user.setCreditcard(InputType.removeSlashes(result.getString("creditcard")));
        user.setAddress((UUID) result.getObject("address"));
        return user;
    }

    public ArrayList<UserDTO> findAll() throws SQLException {
        ArrayList<UserDTO> userList = new ArrayList<UserDTO>();
        Statement query = getStatement();
        if (query == null)
            return null;
        ResultSet result = query.executeQuery("SELECT * FROM users ORDER BY username ASC");
        while (result.next()) {
            UserDTO user = fromResult(result);
            userList.add(user);
        }
        return userList;
    }

    public UserDTO findUserByUsername(String username) throws SQLException {
        if(username == null){
            logger.warning("Tried to find by null username");
            return null;
        }
        Statement query = getStatement();
        
        ResultSet result = query.executeQuery(String.format("SELECT * FROM users WHERE username='%s'",
                                                            username));
        if (!result.next()) {
            logger.warning("No record for " + username + " is found.");
            return null;
        }
        UserDTO user = fromResult(result);
        if (result.next())
            logger.severe("More than one record for "
                          + username
                          + "is found. Check database for consistency. Returning first record");
        return user;
    }

    /* Returns true if user is successfully added to the db, false otherwise. */
    public void addUser(UserDTO user) throws InsertionCheckException,
                                     SQLException {
        if (!preinsertionCheck(user))
            throw new InsertionCheckException();

        String insertion = "INSERT INTO users(username, password, salt, email, nickname, firstname, lastname, yob, confirm, banned, creditcard, address) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement query = getPreparedStatement(insertion);

        int fieldIndex = 0;
        query.setString(++fieldIndex, user.getUsername());
        query.setString(++fieldIndex, user.getPassword());
        query.setString(++fieldIndex, user.getSalt());
        query.setString(++fieldIndex, InputType.addSlashes(user.getEmail()));

        if (user.getNickname() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex,
                            InputType.addSlashes(user.getNickname()));

        if (user.getFirstname() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex,
                            InputType.addSlashes(user.getFirstname()));

        if (user.getLastname() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex,
                            InputType.addSlashes(user.getLastname()));

        query.setInt(++fieldIndex, user.getYob());
        query.setBoolean(++fieldIndex, user.getConfirm());
        query.setBoolean(++fieldIndex, user.getBanned());

        if (user.getCreditcard() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex,
                            InputType.addSlashes(user.getCreditcard()));

        if (user.getAddress() == null)
            query.setNull(++fieldIndex, Types.OTHER);
        else
            query.setObject(++fieldIndex, user.getAddress());

        query.executeUpdate();

    }

    public void updateUser(UserDTO user) throws InsertionCheckException, SQLException {
        if (!preinsertionCheck(user))
            throw new InsertionCheckException();

        String update = "UPDATE users "
                        + "SET password=?,salt=?,email=?,"
                        + "nickname=?,firstname=?,lastname=?,"
                        + "yob=?, confirm=?, banned=?, creditcard=?,"
                        + "address=? WHERE username=?";
        PreparedStatement query = getPreparedStatement(update);

        int fieldIndex = 0;
        query.setString(++fieldIndex, user.getPassword());
        query.setString(++fieldIndex, user.getSalt());
        query.setString(++fieldIndex, InputType.addSlashes(user.getEmail()));

        if (user.getNickname() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex,
                            InputType.addSlashes(user.getNickname()));

        if (user.getFirstname() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex,
                            InputType.addSlashes(user.getFirstname()));

        if (user.getLastname() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex,
                            InputType.addSlashes(user.getLastname()));

        query.setInt(++fieldIndex, user.getYob());
        query.setBoolean(++fieldIndex, user.getConfirm());
        query.setBoolean(++fieldIndex, user.getBanned());

        if (user.getCreditcard() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex,
                            InputType.addSlashes(user.getCreditcard()));

        if (user.getAddress() == null)
            query.setNull(++fieldIndex, Types.OTHER);
        else
            query.setObject(++fieldIndex, user.getAddress());

        query.setString(++fieldIndex, user.getUsername());

        query.executeUpdate();
    }

    private boolean preinsertionCheck(UserDTO user) {
        if (user.getUsername() == null) {
            logger.warning("Insertion aborted, user has null username");
            return false;
        } else if (user.getPassword() == null) {
            logger.warning("Insertion aborted, user has null password");
            return false;
        } else if (user.getSalt() == null) {
            logger.warning("Insertion aborted, user has null salt");
            return false;
        }

        else if (user.getEmail() == null) {
            logger.warning("Insertion aborted, user has null email");
            return false;
        }
        return true;
    }

    public boolean exists(String username) {
        try {
            return findUserByUsername(username) != null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
