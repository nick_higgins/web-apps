/**
 *
 */
package edu.unsw.comp9321.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import edu.unsw.comp9321.dto.AddressDTO;
import edu.unsw.comp9321.inputvalidation.InputType;

/**
 * @author Timothy Masters
 */
public class AddressDAO extends GenericDAO {

    public AddressDAO() {
        super();
    }

    private AddressDTO fromRequest(ResultSet result) throws SQLException {
        AddressDTO addr = new AddressDTO();
        addr.setUuid((UUID) result.getObject("addrId"));
        if (result.getString("addrStreet") != null)
            addr.setStreet(result.getString(InputType.removeSlashes("addrStreet")));
        if (result.getString("addrCity") != null)
            addr.setCity(result.getString(InputType.removeSlashes("addrCity")));
        if (result.getString("addrState") != null)
            addr.setState(result.getString(InputType.removeSlashes("addrState")));
        if (result.getString("addrCountry") != null)
            addr.setCountry(result.getString(InputType.removeSlashes("addrCountry")));
        try {
            addr.setPostcode(Integer.parseInt(result.getString("addrPostalCode")));
        } catch (NumberFormatException e) {
            addr.setPostcode(0);
        }
        return addr;
    }

    public AddressDTO getAddress(UUID id) throws SQLException {
        PreparedStatement query = getPreparedStatement("SELECT * FROM address WHERE addrId=?");
        if (query == null)
            return null;
        query.setObject(1, id);
        ResultSet result = query.executeQuery();
        if (!result.next()) {
            logger.warning("No record for the given UUID is found:" + id);
            return null;
        }
        AddressDTO addr = fromRequest(result);
        if (result.next())
            logger.warning("More than one record retrieved for given UUID, returning first record.");
        return addr;
    }

    public void insertAddress(AddressDTO address) throws SQLException {
        PreparedStatement statement = getPreparedStatement("INSERT INTO address (addrID, addrStreet, addrCity, addrState, addrCountry, addrPostalCode)  VALUES (?, ?, ?, ?, ?, ?)");

        int fieldIndex = 0;
        statement.setObject(++fieldIndex, address.getUuid());

        if (address.getStreet() != null)
            statement.setString(++fieldIndex,
                                InputType.addSlashes(address.getStreet()));
        else
            statement.setNull(++fieldIndex, Types.VARCHAR);

        if (address.getCity() != null)
            statement.setString(++fieldIndex,
                                InputType.addSlashes(address.getCity()));
        else
            statement.setNull(++fieldIndex, Types.VARCHAR);

        if (address.getState() != null)
            statement.setString(++fieldIndex,
                                InputType.addSlashes(address.getState()));
        else
            statement.setNull(++fieldIndex, Types.VARCHAR);

        if (address.getCountry() != null)
            statement.setString(++fieldIndex,
                                InputType.addSlashes(address.getCountry()));
        else
            statement.setNull(++fieldIndex, Types.VARCHAR);

        statement.setInt(++fieldIndex, address.getPostcode());

        statement.executeUpdate();
    }

    public void updateAddress(AddressDTO addr) throws SQLException {
        String update = "UPDATE address "
                        + "SET addrStreet=?,addrCity=?,"
                        + "addrState=?,addrCountry=?,addrPostalCode=? "
                        + "WHERE addrId=?";

        PreparedStatement query = getPreparedStatement(update);
        int fieldIndex = 0;
        if (addr.getStreet() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex, addr.getStreet());

        if (addr.getCity() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex, addr.getCity());

        if (addr.getState() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex, addr.getState());

        if (addr.getCountry() == null)
            query.setNull(++fieldIndex, Types.VARCHAR);
        else
            query.setString(++fieldIndex, addr.getCountry());

        query.setInt(++fieldIndex, addr.getPostcode());
        query.setObject(++fieldIndex, addr.getUuid());

        query.executeUpdate();

    }

    public void removeAddress(AddressDTO address) {
        //TODO
    }
}
