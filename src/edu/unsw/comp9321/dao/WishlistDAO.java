package edu.unsw.comp9321.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import edu.unsw.comp9321.dto.WishlistDTO;

public class WishlistDAO extends GenericDAO {
    public WishlistDAO() {
        super();
    }

    private WishlistDTO fromRequest(ResultSet result) throws SQLException {
        WishlistDTO wishlist = new WishlistDTO();
        wishlist.setUsername(result.getString("username"));
        wishlist.setItemID((UUID) result.getObject("itemId"));
        return wishlist;
    }

    public ArrayList<WishlistDTO> getWishlistByUser(String user) throws SQLException {
        ArrayList<WishlistDTO> wishlist = new ArrayList<WishlistDTO>();
        PreparedStatement query = getPreparedStatement("SELECT * FROM wishlist WHERE username=?");
        if (query == null)
            return null;
        query.setString(1, user);
        ResultSet result = query.executeQuery();
        while (result.next())
            wishlist.add(fromRequest(result));

        return wishlist;
    }

    public boolean exists(WishlistDTO wishlist) {
        try {
            PreparedStatement query = getPreparedStatement("SELECT * FROM wishlist WHERE username=? AND itemId=?");
            
            query.setString(1, wishlist.getUsername());
            query.setObject(2, wishlist.getItemID());
            ResultSet result = query.executeQuery();
            return result.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void addWishlist(WishlistDTO wishlist) throws SQLException {
        PreparedStatement statement = getPreparedStatement("INSERT INTO wishlist (username, itemId) VALUES (?, ?)");

        int fieldIndex = 0;
        statement.setString(++fieldIndex, wishlist.getUsername());
        statement.setObject(++fieldIndex, wishlist.getItemID());

        statement.executeUpdate();

    }

    public void removeWishlist(WishlistDTO wishlist) throws SQLException {
        PreparedStatement statement = getPreparedStatement("DELETE FROM wishlist WHERE username=? AND itemId=?");

        int fieldIndex = 0;
        statement.setString(++fieldIndex, wishlist.getUsername());
        statement.setObject(++fieldIndex, wishlist.getItemID());

        statement.executeUpdate();

    }

	public void removeAllItemsWishlist(String itemId) throws SQLException {
		PreparedStatement statement = getPreparedStatement("DELETE FROM wishlist WHERE itemId=?");
        statement.setObject(1, UUID.fromString(itemId));
        statement.executeUpdate();		
	}

}
