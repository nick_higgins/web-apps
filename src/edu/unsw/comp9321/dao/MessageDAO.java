package edu.unsw.comp9321.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;

import edu.unsw.comp9321.dto.MessageDTO;

public class MessageDAO extends GenericDAO {

    public MessageDAO() {
        super();
    }

    public ArrayList<MessageDTO> findAll() throws SQLException {
        return findAll("");
    }

    protected ArrayList<MessageDTO> findAll(String optionalFilter) throws SQLException {
        ArrayList<MessageDTO> messageList = new ArrayList<MessageDTO>();
        Statement query = getStatement();

        String queryString = "SELECT * FROM messages" + optionalFilter;
        logger.info("Executing query: " + queryString);
        ResultSet result = query.executeQuery(queryString);
        while (result.next()) {
            MessageDTO msg = new MessageDTO();
            msg.setMsgId((UUID) result.getObject("msgId"));
            msg.setUsername(result.getString("username"));
            msg.setTitle(result.getString("title"));
            msg.setMessage(result.getString("message"));
            msg.setUnread(result.getBoolean("unread"));
            msg.setGenerationTime((Timestamp) result.getObject("generationTime"));
            messageList.add(msg);
        }
        return messageList;
    }

    public ArrayList<MessageDTO> findAllByUsername(String username) throws SQLException {
        String filter = " WHERE username='" + username + "'";
        return findAll(filter);
    }

    public MessageDTO findUnreadByUsername(String username) throws SQLException {
        PreparedStatement query = getPreparedStatement("SELECT * FROM messages WHERE username=? and unread='true' LIMIT 1");
        if (query == null)
            return null;
        query.setObject(1, username);
        ResultSet result = query.executeQuery();
        if (!result.next())
            return null;
        MessageDTO msg = new MessageDTO();
        msg.setMsgId((UUID) result.getObject("msgId"));
        msg.setUsername(result.getString("username"));
        msg.setTitle(result.getString("title"));
        msg.setMessage(result.getString("message"));
        msg.setUnread(result.getBoolean("unread"));
        return msg;
    }

    public MessageDTO findMessageById(UUID msgId) throws SQLException {
        PreparedStatement query = getPreparedStatement("SELECT * FROM messages WHERE msgId=?");
        if (query == null)
            return null;
        query.setObject(1, msgId);
        ResultSet result = query.executeQuery();
        if (!result.next()) {
            logger.warning("No record for the given UUID is found.");
            return null;
        }
        MessageDTO msg = new MessageDTO();
        msg.setMsgId((UUID) result.getObject("msgId"));
        msg.setUsername(result.getString("username"));
        msg.setTitle(result.getString("title"));
        msg.setMessage(result.getString("message"));
        msg.setUnread(result.getBoolean("unread"));
        msg.setGenerationTime((Timestamp) result.getObject("generationTime"));
        if (result.next())
            logger.warning("More than one record retrieved for given UUID, returning first record.");
        return msg;
    }

    public void addMessage(MessageDTO msg) throws InsertionCheckException, SQLException {
        if (!preinsertionCheck(msg))
            throw new InsertionCheckException("Message values can't be null");
        String insertion = "INSERT INTO messages (msgId, username, generationTime, title, message, unread) VALUES(?,?,?,?,?,?)";
        PreparedStatement query = getPreparedStatement(insertion);

        // all checks passed, populate fields in query
        int fieldIndex = 0;
        query.setObject(++fieldIndex, msg.getMsgId());
        query.setString(++fieldIndex, msg.getUsername());
        query.setTimestamp(++fieldIndex, msg.getGenerationTime());
        query.setString(++fieldIndex, msg.getTitle());
        query.setString(++fieldIndex, msg.getMessage());
        query.setBoolean(++fieldIndex, msg.isUnread());
        query.executeUpdate();
    }

    public void updateMessage(MessageDTO msg) throws InsertionCheckException, SQLException {
        if (!preinsertionCheck(msg))
            throw new InsertionCheckException("Message values can't be null");
        String update = "UPDATE messages "
                        + "SET username=?,generationTime=?,title=?,message=?,"
                        + "unread=? WHERE msgId=? ";
        PreparedStatement query = getPreparedStatement(update);

        int fieldIndex = 0;
        query.setString(++fieldIndex, msg.getUsername());
        query.setTimestamp(++fieldIndex, msg.getGenerationTime());
        query.setString(++fieldIndex, msg.getTitle());
        query.setString(++fieldIndex, msg.getMessage());
        query.setBoolean(++fieldIndex, msg.isUnread());
        query.setObject(++fieldIndex, msg.getMsgId());
        query.executeUpdate();
    }

    protected boolean preinsertionCheck(MessageDTO msg) {
        if (msg.getMsgId() == null)
            return false;
        else if (msg.getGenerationTime() == null)
            return false;
        else if (msg.getTitle() == null)
            return false;
        else if (msg.getMessage() == null)
            return false;
        else if (msg.getUsername() == null)
            return false;
        return true;
    }

    public int countUnread(String username) throws SQLException {
        Statement query = getStatement();
        if (query == null)
            return 0;
        String queryString = "SELECT count(msgId) FROM messages WHERE username='"
                             + username
                             + "' AND unread='true'";
        ResultSet result = query.executeQuery(queryString);
        if (!result.next())
            return 0;
        return result.getInt(1);
    }

	public void deleteMessageById(UUID msgId) throws SQLException {
		String delete = "DELETE FROM messages WHERE msgId=?";
		PreparedStatement query = getPreparedStatement(delete);
		query.setObject(1, msgId);        	
        query.executeUpdate();	
	}    
}
