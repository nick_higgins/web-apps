package edu.unsw.comp9321.dao.test;

import static org.junit.Assert.*;
import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.unsw.comp9321.dao.PostgresConnFactory;

public class FactoryUnitTest {

    static private InitialContext ic;
    static private final int maxConnNum = 20;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
            "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES, 
            "org.apache.naming");            

        // Create InitialContext with java:/comp/env/jdbc
        FactoryUnitTest.ic = new InitialContext();
        FactoryUnitTest.ic.createSubcontext("java:");
        FactoryUnitTest.ic.createSubcontext("java:/comp");
        FactoryUnitTest.ic.createSubcontext("java:/comp/env");
        FactoryUnitTest.ic.createSubcontext("java:/comp/env/jdbc");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
        // Construct DataSource
        BasicDataSource  ds = new BasicDataSource();
        ds.setUrl("jdbc:postgresql://localhost:5432/auction_unit_test");
        ds.setUsername("tomcat");
        ds.setPassword("webapp");
        ds.setMaxActive(maxConnNum);
        ds.setMaxIdle(10);
        FactoryUnitTest.ic.bind("java:/comp/env/jdbc/Postgresql", ds);
    }

    @After
    public void tearDown() throws Exception
    {
        FactoryUnitTest.ic.unbind("java:/comp/env/jdbc/Postgresql");
    }


    /* Tests factory singleton instance is correctly initialized. */
    @Test
    public void testCreatePostgresConnFactory()
    {
        assertNotNull(PostgresConnFactory.THE_FACTORY);
    }
    
    @Test
    public void testGetSingleConnection() throws Exception
    {
        Connection conn = null;
        try
        {
            conn = PostgresConnFactory.getConnection();
        }
        catch (Exception err)
        {
            fail("Error in initializing: " + err.getMessage());
        }
        assertNotNull(conn);
        conn.close();
    }
    
    @Test(timeout=10000)
    public void testGetMultipleConnection() throws Exception
    {
        Connection[] conn = new Connection[20];
        for (int i = 0; i < maxConnNum; i++)
            conn[i] = null;
        int counter = 0;
        try
        {
            for (int i = 0; i < maxConnNum; i++)
            {
                counter = i + 1;
                conn[i] = PostgresConnFactory.getConnection();
                if (conn[i] == null)
                     throw new Exception("Connection is null");
            }
        }
        catch (Exception err)
        {
            // close all previous connections first
            for (int i = 0; i < counter - 1; i++)
                conn[i].close();
            fail("Error in initializing connection " + counter + ": " + err.getMessage());
        }
        
        for (int i = 0; i < maxConnNum; i++)
            conn[i].close();
    }
}
