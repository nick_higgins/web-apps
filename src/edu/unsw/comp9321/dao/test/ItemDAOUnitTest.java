package edu.unsw.comp9321.dao.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.unsw.comp9321.dao.ItemDAO;
import edu.unsw.comp9321.dao.ItemDAO.ItemNotFoundException;
import edu.unsw.comp9321.dao.PostgresConnFactory;
import edu.unsw.comp9321.dto.Currency;
import edu.unsw.comp9321.dto.ItemDTO;

public class ItemDAOUnitTest {

    static private InitialContext ic;
    private Connection            conn;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                           "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");

        // Create InitialContext with java:/comp/env/jdbc
        ItemDAOUnitTest.ic = new InitialContext();
        ItemDAOUnitTest.ic.createSubcontext("java:");
        ItemDAOUnitTest.ic.createSubcontext("java:/comp");
        ItemDAOUnitTest.ic.createSubcontext("java:/comp/env");
        ItemDAOUnitTest.ic.createSubcontext("java:/comp/env/jdbc");
        BasicDataSource ds = new BasicDataSource();
        ds.setUrl("jdbc:postgresql://localhost:5432/auction_unit_test");
        ds.setUsername("tomcat");
        ds.setPassword("webapp");
        ds.setMaxActive(20);
        ds.setMaxIdle(10);
        ItemDAOUnitTest.ic.bind("java:/comp/env/jdbc/Postgresql", ds);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {}

    @Before
    public void setUp() throws Exception {
        conn = PostgresConnFactory.getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute("INSERT INTO users ("
                     + "username, "
                     + "password, "
                     + "salt, "
                     + "email) "
                     + " VALUES ('John Doe', 'password123', 'saltsaltsalt' ,'email@email.com')");
        stmt.execute("INSERT INTO items ("
                     + "itemid, "
                     + "owner, "
                     + "title) "
                     + " VALUES ('18d2b062-4e3d-49f4-a100-a793abe86305', 'John Doe', 'Test Item1')");
    }

    @After
    public void tearDown() throws Exception {
        if (conn != null) {
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM items");
            stmt.execute("DELETE FROM users");
            conn.close();
            conn = null;
        }
    }

    @Test
    public void testFindItemById() throws ItemNotFoundException {
        ItemDAO itemDao = new ItemDAO();
        ItemDTO item = null;
        try {
            item = itemDao.findItemById(UUID.fromString("18d2b062-4e3d-49f4-a100-a793abe86305"));
        } catch (SQLException err) {
            // TODO Auto-generated catch block
            err.printStackTrace();
            fail("FindItemByID failed: " + err.getMessage());
        } finally {
            itemDao.close();
        }
        assertEquals("ItemID not correct",
                     "18d2b062-4e3d-49f4-a100-a793abe86305",
                     item.getItemId().toString());
        assertEquals("Owner not correct", "John Doe", item.getOwner());
        assertEquals("Title not correct", "Test Item1", item.getTitle());
    }

    @Test
    public void testAddItem() {
        ItemDAO itemDao = new ItemDAO();
        ItemDTO item = new ItemDTO();
        item.setOwner("John Doe");
        item.setTitle("Test Item");
        item.setCategory("Abstract");
        item.setPicture("www.picture.com");
        item.setDescription("This is a test item");
        item.setReservePrice(150);
        item.setBiddingStartPrice(50);
        item.setBiddingIncrements(10);
        item.setBiddingCurrency(Currency.EUR);
        Date date = new Date();
        item.setEndTime(new Timestamp(date.getTime()));
        item.setIsInAuction(false);
        item.setCurrentBiddingPrice(0);

        try {
            itemDao.AddItem(item);
        } catch (SQLException e) {

            fail("An error occurred when adding new item");
        } finally {
            itemDao.close();
        }

    }

    @Test
    public void testAddItemNoValidOwner() {
        ItemDAO itemDao = new ItemDAO();
        ItemDTO item = new ItemDTO();
        item.setOwner("John Doe 2");
        item.setTitle("Test Item");
        item.setCategory("Abstract");
        item.setPicture("www.picture.com");
        item.setDescription("This is a test item");
        item.setReservePrice(150);
        item.setBiddingStartPrice(50);
        item.setBiddingIncrements(10);
        item.setBiddingCurrency(Currency.EUR);
        Date date = new Date();
        item.setEndTime(new Timestamp(date.getTime()));
        item.setIsInAuction(false);
        item.setCurrentBiddingPrice(0);

        try {
            itemDao.AddItem(item);

            fail("Add item with invalid owner should have failed but didn't");
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testAddItemMinInfo() {
        ItemDAO itemDao = new ItemDAO();
        ItemDTO item = new ItemDTO();
        ItemDTO newItem = null;
        item.setOwner("John Doe");
        item.setTitle("Test Item");
        try {
            itemDao.AddItem(item);
        } catch (SQLException err) {
            itemDao.close();
            err.printStackTrace();
            fail("An error occurred when adding new item with min info: "
                 + err.getMessage());
        }
        try {
            newItem = itemDao.findItemById(item.getItemId());
        } catch (SQLException err) {
            // TODO Auto-generated catch block
            err.printStackTrace();
            fail("get inserted min info item failed: " + err.getMessage());
        } catch (ItemNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            fail("get inserted min info item failed: " + e.getMessage());
        } finally {
            itemDao.close();
        }
        assertEquals("Owner is note correct", "John Doe", newItem.getOwner());
        assertEquals("Title is note correct", "Test Item", newItem.getTitle());
        assertNull(newItem.getCategory());
        assertNull(newItem.getPicture());
        assertNull(newItem.getDescription());
        assertNull(newItem.getAddress());
        assertNull(newItem.getBiddingCurrency());
        assertNull(newItem.getEndTime());
        assertNull(newItem.getCurrentBidder());
    }

    @Test
    public void testSearchItems() {
        ItemDAO itemDao = new ItemDAO();
        ItemDTO item = new ItemDTO();
        ArrayList<ItemDTO> matchedItems = null;
        item.setOwner("John Doe");
        try {
            for (int i = 0; i < 100; i++) {
                item.setItemId(UUID.randomUUID());
                String itemname = "Clock";
                if (i % 10 == 0)
                    itemname = "Watch";
                item.setTitle(itemname + i);
                itemDao.AddItem(item);
                
            }
        } catch (Exception err) {
            itemDao.close();
            fail("An error occurred when adding up to 100 items: "
                 + err.getMessage());
        }
        try {
            matchedItems = itemDao.searchItems("atch");
        } catch (SQLException err) {
            // TODO Auto-generated catch block
            err.printStackTrace();
            fail("get inserted min info item failed: " + err.getMessage());
        } finally {
            itemDao.close();
        }
        assertEquals("Wrong number of items returned", 10, matchedItems.size());
        for (int i = 0; i < matchedItems.size(); i++) {
            String name = matchedItems.get(i).getTitle();
            if (!name.startsWith("Watch")) {
                fail("Title of item returned does not match search string.");
            }
        }
    }
}
