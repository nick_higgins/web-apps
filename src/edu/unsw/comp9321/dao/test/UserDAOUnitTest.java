package edu.unsw.comp9321.dao.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.Statement;
import java.util.UUID;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.unsw.comp9321.dao.PostgresConnFactory;
import edu.unsw.comp9321.dao.UserDAO;
import edu.unsw.comp9321.dto.UserDTO;

public class UserDAOUnitTest {

    static private InitialContext ic;
    private Connection conn;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES, 
                "org.apache.naming");            

        // Create InitialContext with java:/comp/env/jdbc
        UserDAOUnitTest.ic = new InitialContext();
        UserDAOUnitTest.ic.createSubcontext("java:");
        UserDAOUnitTest.ic.createSubcontext("java:/comp");
        UserDAOUnitTest.ic.createSubcontext("java:/comp/env");
        UserDAOUnitTest.ic.createSubcontext("java:/comp/env/jdbc");
        BasicDataSource  ds = new BasicDataSource();
        ds.setUrl("jdbc:postgresql://localhost:5432/auction_unit_test");
        ds.setUsername("tomcat");
        ds.setPassword("webapp");
        ds.setMaxActive(20);
        ds.setMaxIdle(10);
        UserDAOUnitTest.ic.bind("java:/comp/env/jdbc/Postgresql", ds);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
        conn = PostgresConnFactory.getConnection();
        Statement stmt =  conn.createStatement();
        stmt.execute("INSERT INTO users ("
                + "username, "
                + "password, "
                + "salt, "
                + "email) "
                + " VALUES ('John Doe', 'password123', 'saltsaltsalt' ,'email@email.com')");
        /*
        stmt.executeQuery("INSERT INTO items ("
                           + "itemid, "
                           + "owner, "
                           + "title) "
                           + " VALUES ('18d2b062-4e3d-49f4-a100-a793abe86305', 'John Doe', 'Test Item1')");
        */
    }

    @After
    public void tearDown() throws Exception
    {
        if (conn != null)
        {
            Statement stmt =  conn.createStatement();
            stmt.execute("DELETE FROM users");
            conn.close();
            conn = null;
        }
    }

    @Test
    public void testAddUser()
    {
        UserDAO userDao = new UserDAO();
        UserDTO user = new UserDTO();
        user.setUsername("John Doh " + UUID.randomUUID().toString());
        
        user.setPassword("Password123");
        user.setSalt("sea_salt");
        user.setEmail("john_doe@internet.com");
        user.setNickname("Johnny");
        user.setFirstname("John");
        user.setLastname ("Doe");
        user.setYob(2000);
        user.setCreditcard("01234567890123456789");
        user.setAddress(null);
        try
        {
            userDao.addUser(user);
        }
        catch (Exception err)
        {
            fail("Error in UserDAO.addUser(): " + err.getMessage());
        }
    }
    
    @Test
    public void testAddUserMinInfo()
    {
        UserDAO userDao = new UserDAO();
        UserDTO user = new UserDTO();
        user.setUsername("John Doh " + UUID.randomUUID().toString());
        
        user.setPassword("Password123");
        user.setSalt("sea_salt");
        user.setEmail("john_doe@internet.com");
        try
        {
            userDao.addUser(user);
                //fail("UserDAO.addUser() with min info returned false. check log for detail");
        }
        catch (Exception err)
        {
            fail("Error in UserDAO.addUser(): " + err.getMessage());
        }
    }
    
    @Test
    public void testAddUserWithInvalidAddress() throws Exception
    {
        UserDAO userDao = new UserDAO();
        UserDTO user = new UserDTO();
        user.setUsername("John Doh " + UUID.randomUUID().toString());
        user.setPassword("Password1234");
        user.setSalt("sea_salt");
        user.setEmail("john_doe@internet.com");
        user.setNickname("Johnny");
        user.setFirstname("John");
        user.setLastname ("Doe");
        user.setYob(2000);
        user.setCreditcard("abcd efgh ijkl mnop");
        user.setAddress(UUID.randomUUID());
        try
        {
            userDao.addUser(user);
            //{
                // expect insertion to fail and return false
                fail("Add user that reference invalid address should not succeed");
            //}
        }
        catch (Exception err)
        {
            // Other exception, not expected.
            //fail("Error in UserDAO.addUser(): " + err.getMessage());
        }
    }
    
    @Test
    public void testGetUserByID() throws Exception
    {
        UserDAO userDao = new UserDAO();
        UserDTO user = userDao.findUserByUsername("John Doe");
        assertEquals("Username not correct", "John Doe", user.getUsername());
        assertEquals("Password not correct", "password123", user.getPassword());
        assertEquals("Salt not correct", "saltsaltsalt", user.getSalt());
        assertEquals("Email not correct", "email@email.com", user.getEmail());
        assertNull(user.getNickname());
        assertNull(user.getFirstname());
        assertNull(user.getLastname());
        assertNull(user.getCreditcard());
        assertNull(user.getAddress());
    }
    
    @Test
    public void testUpdateUser() throws Exception
    {
        UserDAO userDao = new UserDAO();
        UserDTO user = userDao.findUserByUsername("John Doe");
        user.setEmail("mynewemail@mail.com");
        userDao.updateUser(user);
        UserDTO newUser = userDao.findUserByUsername("John Doe");
        assertEquals("Email not correctly updated", "mynewemail@mail.com", newUser.getEmail());
    }

}
